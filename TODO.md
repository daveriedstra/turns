# TODO

* **housekeeping**
* [ ] fail nicely if >4 encoders configured in a page (likely means turns.toml is misconfigured)
* [ ] remove vec allocations from the loop
* [ ] ar interpolation time should be based on effective kr refresh rate (in eg level and value modes)
* [ ] decouple kr refresh rate and LED update rate (eg to avoid flooding OSC for oscillating outputs)
* [ ] more descriptive port & OSC names per mode outlet
* [ ] "cv" transport should probably be called "jack" to be consistent
* [ ] cross-compile windows, mac, & ARM releases in CI (**help wanted**)
* [ ] optionally use f32 samples for eg rpi compat
* [ ] DASP compat? (phasor frequency changes reset phase...)

* **general features**
* [ ] inputs
    * [ ] seq!
    * [ ] FM AR inputs for oscillating modes
    * [ ] auto OSC inputs for relevant config options somehow?
* [ ] MIDI improvements
    * [ ] double check midi shapes make sense
    * [ ] MIDI note output
    * [ ] MIDI 14-bit output
    * [ ] configurable MIDI CC numbers per mode outlet
    * [ ] configurable channel in & out
    * [ ] better approach to selecting MIDI ports?
* [ ] OSC improvements
    * [ ] configurable OSC address per mode outlet
* [ ] configurable slew & curve for Delta
* [ ] Pager improvements
    * [ ] OSC pager by default? or both pagers?
    * [ ] pager.py should use `--config` instead of `--file` for consistency
    * [ ] pager.py should look for config file with same behaviour as `turns` (ie, check default location if none specified or found in cwd)
    * [ ] pager should have a MIDI mode for two CCs, one to step up and one to step down
    * [ ] midi pager should have better config options for multiple CCs (eg, for non-adjacent values)
    * [ ] midi pager in addressed-value should have a normalize feature which spreads the functional page range over the input value range instead of using precise values (so an encoder "just works" with eg four pages)
    * [ ] pager should be more accessible
    * [ ] pager could (should?) turn into a whole CLI app that can configure, start, page, reconfigure, save configs

* **new modes**
* [ ] quadlfo-2 with regular LFO-like controls, inputs for phase and freq ratio, and configurable initial values (which quadlfo should really have too). Outputs for LFOs and intersections.
* [ ] new mode: buffer-scan (or scrub or something) which takes a buffer rendering input and can rotate it, outputs val pretty much
* [ ] new mode: throw-FO? LFThrow? which is an LFO with configurable bounds and the ability to stop (or at least radically slow) at a position
