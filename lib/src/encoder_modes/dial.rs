use crate::encoder_mode::EncoderMode;
use crate::ring::RingLed;

pub struct Dial {
    pub val: isize,
    raw_val: f64,
    shuttle_size: usize,
    num_stops: usize,
    idx: usize,
}

impl Iterator for Dial {
    type Item = f64;

    fn next(&mut self) -> Option<f64> {
        if self.idx > 0 {
            None
        } else {
            self.idx += 1;
            Some(self.val as f64)
        }
    }
}

impl EncoderMode for Dial {
    fn new(_: usize, _: usize) -> Self {
        let mut s = Self {
            raw_val: 0.,
            val: 0,
            shuttle_size: 0,
            num_stops: 0,
            idx: 0,
        };
        s.set_num_stops(4);

        s
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 1 }
    fn set_sample_rate(&mut self, _: usize) {}
    fn set_block_size(&mut self, _: usize) {}

    fn delta(&mut self, delta: i32) {
        self.raw_val += delta as f64 / 832.;
        self.val = (self.raw_val * self.num_stops as f64).round() as isize;
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        self.idx = 0;
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        self.make_shuttle()
    }
}

impl Dial {
    /// Use this to set how many stops on the Dial there are
    pub fn set_num_stops(&mut self, num_stops: usize) {
        let shuttle_size = (64. / num_stops as f64).round() as usize;
        self.shuttle_size = shuttle_size.clamp(2, 64);
        self.num_stops = num_stops;
    }

    fn make_shuttle(&mut self) -> Vec<RingLed> {
        let mut start = self.val as f64 / self.num_stops as f64 * 64.;
        start -= 32. / self.num_stops as f64;
        while start < 0. {
            start += 64.;
        }

        let mut shuttle = Vec::new();
        for i in 0..self.shuttle_size {
            let pos = (start + i as f64) % 64.;
            shuttle.push((pos, 15));
        }
        shuttle
    }
}
