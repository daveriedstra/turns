use crate::encoder_mode::EncoderMode;
use crate::line::Line;
use crate::ring::RingLed;
use crate::shape::Shape;

/**
 * A Mode which outputs delta values scaled to unit. Currently linear and not clamped to unit.
 * TODO: configurable slew & curve...?
 */
pub struct Delta {
    /**
     * The publically displayed value. This will have slew and curve eventually.
     */
    pub val: f64,
    unscaled_val: f64,
    /**
     * The raw value accumulator.
     */
    accumulator: f64,
    line: Line,
    rise_time: f64,
    fall_time: f64,
    /**
     * Value of delta which will be output as a 1.0. Default is 64.0. Use for tuning.
     */
    pub unit_size: f64,
    idx: usize,
    pub shape: Shape,
}

impl Iterator for Delta {
    type Item = f64;

    fn next(&mut self) -> Option<f64> {
        if self.idx > 0 {
            None
        } else {
            self.idx += 1;
            self.accumulator = 0.;
            Some(self.val as f64)
        }
    }
}

impl EncoderMode for Delta {
    fn new(sample_rate: usize, _: usize) -> Self {
        Self {
            unit_size: 64.,
            val: 0.,
            unscaled_val: 0.,
            accumulator: 0.,
            line: Line::new(sample_rate),
            rise_time: 150.,
            fall_time: 150.,
            idx: 0,
            shape: Shape::default(),
        }
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 1 }
    fn set_sample_rate(&mut self, new_sr: usize) {
        self.line.set_sample_rate(new_sr);
    }
    fn set_block_size(&mut self, _: usize) {}

    fn delta(&mut self, delta: i32) {
        self.accumulator += delta as f64 / self.unit_size;
        self.line.start(self.accumulator, self.rise_time);
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        self.idx = 0;
        self.unscaled_val = self.line.tick();
        // if the Line is stopped, start falling back to zero
        if (self.unscaled_val - self.line.dest).abs() < f64::EPSILON && self.accumulator == 0. {
            self.line.start(0., self.fall_time);
        }
        self.val = self.shape.linear_to_scale(self.unscaled_val);
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        let len = self.unscaled_val.signum() * self.unscaled_val.abs().powf(0.5) * 64.; // pow and maintain sign
        let counter_clockwise = len.is_sign_negative();

        // fill list and apply fade pattern, invert if CCW
        let len = len.abs().min(64.).floor();
        let fade_start = (len * self.line.get_progress()).floor();
        let fade_len = (len - fade_start).floor();

        (0..len as usize)
            .into_iter()
            .map(|i| {
                let mut n = (i as f64 + 64.) % 64.; // just wrap
                let mut brightness = 15;

                // pretty fade pattern:
                // fade from the progress point until the end (eg so at the start of the motion,
                // the whole extent will be fade, at halfway, only half will be faded, and at the
                // end, none will)
                if fade_len >= 1. && n >= fade_start {
                    let point_in_fade = (n - fade_start) / fade_len;
                    brightness = (15. - point_in_fade * 15.).floor() as usize;
                }

                // invert direction as needed
                if counter_clockwise {
                    n = 64. - n;
                }

                (n, brightness)
            })
        .collect()
    }
}
