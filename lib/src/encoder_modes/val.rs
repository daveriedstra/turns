use crate::encoder_mode::EncoderMode;
use crate::line::Line;
use crate::ring::RingLed;
use crate::shape::Shape;

pub struct Val {
    val: f64,
    unscaled_val: f64,
    line: Line,
    smoothing_time: f64,
    idx: usize,
    pub shape: Shape,
}

impl Iterator for Val {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        if self.idx > 0 {
            None
        } else {
            self.idx += 1;
            Some(self.val)
        }
    }
}

impl EncoderMode for Val {
    fn new(sample_rate: usize, _: usize) -> Self {
        Self {
            val: 0.,
            unscaled_val: 0.,
            line: Line::new(sample_rate),
            smoothing_time: 30.,
            idx: 0,
            shape: Shape::default(),
        }
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 1 }
    fn set_block_size(&mut self, _: usize) {}

    fn set_sample_rate(&mut self, new_sr: usize) {
        self.line.set_sample_rate(new_sr);
    }

    fn delta(&mut self, delta: i32) {
        let dest = self.line.dest + delta as f64 / 832.;
        self.line.start(dest, self.smoothing_time);
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        self.unscaled_val = self.line.tick();
        self.val = self.shape.linear_to_scale(self.unscaled_val);
        self.idx = 0;
    }

    /**
     * Value is shown as fraction of a full rotation (ie, 0 at noon, 0.5 at 6, 1 at noon, etc),
     * multiple (ie number of full rotations) as a tally in the respective direction (positive is
     * clockwise, negative is counter-clockwise).
     *
     * Tally is given in groups of 3 LEDs, the last of which is off, eg: |█|█| |█|█| |█|█| |
     * This allows 2 full rotations' worth of tallies to be displayed.
     */
    fn get_leds(&mut self) -> Vec<RingLed> {
        const TALLY_INTENSITY: usize = 5;
        let pos = self.unscaled_val * 64.;
        let tally = self.unscaled_val.abs().floor() as usize;
        let sign = self.unscaled_val.signum(); // -1 or 1

        let mut leds = vec![
            // position marker
            (pos - 0.5, 15),
            (pos + 0.5, 15),
            // noon stays lit
            (0., TALLY_INTENSITY),
            // direction marker for -1 < x < 1
            (sign, TALLY_INTENSITY),
            (2. * sign, TALLY_INTENSITY),
        ];

        // show tally in groups of 3 LEDs, the last of which is off
        for i in 0..tally {
            let block = (i as f64 + 1.) * 3. * sign + (3. * sign);
            leds.push((block - 2. * sign, TALLY_INTENSITY));
            leds.push((block - sign, TALLY_INTENSITY));
        }

        leds
    }
}
