use crate::encoder_mode::EncoderMode;
use crate::phasor::Phasor;
use crate::ring::RingLed;
use crate::ar::Ar;
use crate::consts::RISING_EDGE_MS;

pub struct Seq {
    phasor: Phasor,
    freq: f64,
    stops: Vec<f64>,
    trig: Ar,
    value: f64,
    idx: usize,
    is_kr: bool,
}

impl Iterator for Seq {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        if self.idx > 0 {
            None
        } else {
            self.idx += 1;
            Some(self.value)
        }
    }
}

impl EncoderMode for Seq {
    fn new(sample_rate: usize, _block_size: usize) -> Self {
        Self {
            phasor: Phasor::new(sample_rate),
            freq: 0.,
            stops: Vec::new(),
            trig: Ar::new(sample_rate),
            value: 0.,
            idx: 0,
            is_kr: sample_rate < 22000,
        }
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 1 }
    fn set_block_size(&mut self, _: usize) {}

    fn set_sample_rate(&mut self, new_sr: usize) {
        self.phasor.set_sample_rate(new_sr);
        self.trig.set_sample_rate(new_sr);
    }

    fn delta(&mut self, delta: i32) {
        self.freq += delta as f64 / (1024.);
        self.phasor.set_freq(self.freq);
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        let phase = self.phasor.tick();
        let abs_ival = self.phasor.interval.abs();
        let mut intersection = false;

        for &mut stop in self.stops.iter_mut() {
            let relative_val = (stop + phase) % 1.;

            // stop should bang if it's within an interval of 0
            if relative_val > -abs_ival && relative_val < abs_ival {
                intersection = true;
            }
        }

        if self.is_kr {
            self.value = intersection as i32 as f64;
        } else {
            if intersection {
                self.trig.start(RISING_EDGE_MS, RISING_EDGE_MS);
            }
            self.value = self.trig.tick();
        }
        self.idx = 0;
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        let mut leds = vec![(0., 7)]; // north
        for &mut stop in self.stops.iter_mut() {
            let pos = (stop + self.phasor.val) % 1. * 64.;
            leds.push((pos, 15));
        }
        leds
    }
}

impl Seq {
    pub fn add_stop(&mut self) {
        self.stops.push(self.freq); // should this be self.phasor.val?
    }

    pub fn set_freq(&mut self, new_freq: f64) {
        self.phasor.set_freq(new_freq);
    }
}
