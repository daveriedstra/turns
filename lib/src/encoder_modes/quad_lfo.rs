use crate::encoder_mode::EncoderMode;
use crate::momentum_osc::MomentumOsc;
use crate::phasor::get_block_dur;
use crate::ring::RingLed;
use crate::wave_shape::WaveShape;
use crate::ar::Ar;
use crate::consts::RISING_EDGE_MS;
use crate::intersect::Intersect;

/**
 * Four LFOs, two clockwise, two counter-clockwise, frequencies set by rate of
 * spin in each direction and the multiplier property. Antialiased by default,
 * optionally not.
 */
pub struct QuadLfo {
    pub multiplier: f64,
    sample_rate: usize,
    block_size: usize,
    block_duration: f64,
    cw: [MomentumOsc; 2],
    ccw: [MomentumOsc; 2],
    trig: Ar,
    antialias: bool,
    values: [f64; 5],
    idx: usize,
    intersect: Intersect::<4>,
    is_kr: bool,
}

impl Iterator for QuadLfo {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        if self.idx > 4 {
            None
        } else {
            let val = self.values[self.idx];
            self.idx += 1;
            Some(val)
        }
    }
}

impl EncoderMode for QuadLfo {
    fn new(sample_rate: usize, block_size: usize) -> Self {
        let mut cw = [
            MomentumOsc::new(sample_rate, block_size),
            MomentumOsc::new(sample_rate, block_size),
        ];
        let mut ccw = [
            MomentumOsc::new(sample_rate, block_size),
            MomentumOsc::new(sample_rate, block_size),
        ];
        cw[1].brightness = 6;
        ccw[1].brightness = 6;

        Self {
            multiplier: 0.5,
            sample_rate,
            block_size,
            block_duration: get_block_dur(sample_rate, block_size),
            cw,
            ccw,
            trig: Ar::new(sample_rate),
            antialias: true,
            values: [0.; 5],
            idx: 0,
            intersect: Intersect::<4>::new(),
            is_kr: sample_rate < 22000,
        }
    }

    fn get_input_count(&mut self) -> usize { 1 }
    fn get_output_count(&mut self) -> usize { 5 }

    fn set_sample_rate(&mut self, new_sr: usize) {
        self.sample_rate = new_sr;
        self.all_oscs(|osc: &mut MomentumOsc| {
            osc.phasor.set_sample_rate(new_sr);
        });
        self.trig.set_sample_rate(new_sr);
        self.set_block_size(self.block_size);
    }

    fn set_block_size(&mut self, new_bs: usize) {
        self.block_size = new_bs;
        let d = get_block_dur(self.sample_rate, new_bs);
        self.block_duration = d;
        self.all_oscs(|o: &mut MomentumOsc| {
            o.set_block_duration(d);
        });
    }

    fn delta(&mut self, delta: i32) {
        if delta > 0 {
            self.cw[0].new_delta(delta);
        } else {
            self.ccw[0].new_delta(delta);
        }
    }

    fn tick(&mut self, mut input: Option<std::slice::Iter<Self::Item>>) {
        let mut in_val: f64 = 0.;

        if let Some(iter) = input.as_mut() {
            in_val = *iter.next().unwrap_or(&0.);
        };

        // keep phasors in sync and update at audio rate
        let mult = self.multiplier + in_val;

        let new_freq = self.cw[0].phasor.get_freq() * mult;
        self.cw[1].phasor.set_freq(new_freq);

        let new_freq = self.ccw[0].phasor.get_freq() * mult;
        self.ccw[1].phasor.set_freq(new_freq);

        // get osc values
        let osc_vals = self.all_oscs(|osc: &mut MomentumOsc| osc.tick());

        // determine intersect trigger
        let intersect = self.intersect.tick(osc_vals);

        if self.is_kr {
            self.values = [
                osc_vals[0],
                osc_vals[1],
                osc_vals[2],
                osc_vals[3],
                intersect as i32 as f64,
            ];
        } else {
            if intersect {
                self.trig.start(RISING_EDGE_MS, RISING_EDGE_MS);
            }

            self.values = [
                osc_vals[0],
                osc_vals[1],
                osc_vals[2],
                osc_vals[3],
                self.trig.tick(),
            ];
        }

        self.idx = 0;
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        // tally count / block duration
        self.all_oscs(|osc: &mut MomentumOsc| {
            osc.update_freq();
        });

        // leds
        if self.antialias {
            self.all_oscs(|osc: &mut MomentumOsc| osc.get_antialiased_leds())
                .into()
        } else {
            self.all_oscs(|osc: &mut MomentumOsc| osc.get_led()).into()
        }
    }
}

impl QuadLfo {
    // Do the same operation for all the oscillators, returning an array of the results
    fn all_oscs<F, T>(&mut self, mut op: F) -> [T; 4]
    where
        F: FnMut(&mut MomentumOsc) -> T,
    {
        [
            op(&mut self.cw[0]),
            op(&mut self.cw[1]),
            op(&mut self.ccw[0]),
            op(&mut self.ccw[1]),
        ]
    }

//     // i and j are both value - interval pairs
//     // returns true if i and j are both within each other's interval
//     fn do_phasors_intersect(i: &(f64, f64), j: &(f64, f64)) -> bool {
//         let mut term_i = i.0;
//         let mut term_j = j.0;

//         // rotate both terms 180° if either are near edge
//         if term_i < i.1 || term_i > (1. - i.1) || term_j < j.1 || term_j > (1. - j.1) {
//             term_i = (term_i + 0.5) % 1.;
//             term_j = (term_j + 0.5) % 1.;
//         }
//         (term_i >= term_j && term_i < (term_j + j.1))
//             || (term_j >= term_i && term_j < (term_i + i.1))
//     }

//     // Takes an array representing the values and intervals of each of the phasors
//     fn is_intersect(mut vals: [(f64, f64); 4]) -> bool {
//         // correct for direction
//         for val in vals.iter_mut() {
//             if val.0 < 0. {
//                 val.0 += 1.;
//             }
//             val.1 = val.1.abs();
//         }

//         for i in 0..4 {
//             for j in 0..4 {
//                 if i != j && QuadLfo::do_phasors_intersect(&vals[i], &vals[j]) {
//                     return true;
//                 }
//             }
//         }
//         false
//     }

    pub fn set_wave_shape(&mut self, wave_shape: WaveShape) {
        self.all_oscs(|osc| {
            osc.set_wave_shape(wave_shape);
        });
    }
}
