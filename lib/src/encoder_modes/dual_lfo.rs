use crate::encoder_mode::EncoderMode;
use crate::momentum_osc::MomentumOsc;
use crate::phasor::get_block_dur;
use crate::wave_shape::WaveShape;
use crate::ring::RingLed;

/**
 * Two LFOs, one clockwise, one counter-clockwise, frequencies set by rate of
 * spin in each direction. Antialiased by default, optionally not.
 */
pub struct DualLfo {
    sample_rate: usize,
    block_size: usize,
    block_duration: f64,
    cw: MomentumOsc,
    ccw: MomentumOsc,
    antialias: bool,
    values: [f64; 2],
    idx: usize,
}

impl Iterator for DualLfo {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        if self.idx > 1 {
            None
        } else {
            let val = self.values[self.idx];
            self.idx += 1;
            Some(val)
        }
    }
}

impl EncoderMode for DualLfo {
    fn new(sample_rate: usize, block_size: usize) -> Self {
        Self {
            sample_rate,
            block_size,
            block_duration: get_block_dur(sample_rate, block_size),
            cw: MomentumOsc::new(sample_rate, block_size),
            ccw: MomentumOsc::new(sample_rate, block_size),
            antialias: true,
            values: [0., 0.],
            idx: 0,
        }
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 2 }

    fn set_sample_rate(&mut self, new_sr: usize) {
        self.sample_rate = new_sr;
        self.cw.phasor.set_sample_rate(new_sr);
        self.ccw.phasor.set_sample_rate(new_sr);
        self.set_block_size(self.block_size);
    }

    fn set_block_size(&mut self, new_bs: usize) {
        self.block_size = new_bs;
        self.block_duration = get_block_dur(self.sample_rate, new_bs);
        self.cw.set_block_duration(self.block_duration);
        self.ccw.set_block_duration(self.block_duration);
    }

    fn delta(&mut self, delta: i32) {
        if delta > 0 {
            self.cw.new_delta(delta);
        } else {
            self.ccw.new_delta(delta);
        }
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        self.values[0] = self.cw.tick();
        self.values[1] = self.ccw.tick();
        self.idx = 0;
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        // tally count / block duration
        self.cw.update_freq();
        self.ccw.update_freq();

        // leds
        if self.antialias {
            return vec![
                self.cw.get_antialiased_leds(),
                self.ccw.get_antialiased_leds(),
            ];
        } else {
            return vec![self.cw.get_led(), self.ccw.get_led()];
        }
    }
}

impl DualLfo {
    pub fn set_wave_shape(&mut self, wave_shape: WaveShape) {
        self.cw.set_wave_shape(wave_shape);
        self.ccw.set_wave_shape(wave_shape);
    }
}
