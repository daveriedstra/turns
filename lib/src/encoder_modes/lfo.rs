use crate::encoder_mode::EncoderMode;
use crate::ring::RingLed;
use crate::phasor::Phasor;
use crate::wave_shape::WaveShape;

pub struct Lfo {
    phasor: Phasor,
    freq: f64,
    val: f64,
    idx: usize,
    wave_shape: WaveShape,
}

impl Iterator for Lfo {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        if self.idx > 0 {
            None
        } else {
            self.idx += 1;
            Some(self.val)
        }
    }
}

impl EncoderMode for Lfo {
    fn new(sample_rate: usize, _block_size: usize) -> Self {
        Self {
            phasor: Phasor::new(sample_rate),
            freq: 0.,
            val: 0.,
            idx: 0,
            wave_shape: WaveShape::Sine,
        }
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 1 }
    fn set_block_size(&mut self, _: usize) {}

    fn set_sample_rate(&mut self, new_sr: usize) {
        self.phasor.set_sample_rate(new_sr);
    }

    fn delta(&mut self, delta: i32) {
        self.freq += delta as f64 / (832.); // 13 * 64
        self.phasor.set_freq(self.freq);
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        let phase = self.phasor.tick();
        self.val = self.wave_shape.phase_to_wave(phase);
        self.idx = 0;
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        let led_val = (self.val * 15.) as usize;
        (0..64).map(|i| (i as f64, led_val)).collect()
    }
}

impl Lfo {
    pub fn set_freq(&mut self, new_freq: f64) {
        self.phasor.set_freq_ramp(new_freq, 2.);
    }

    pub fn set_wave_shape(&mut self, wave_shape: WaveShape) {
        self.wave_shape = wave_shape;
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn lfo_get_leds_returns_full_ring() {
        let mut lfo = Lfo::new(4, 1);
        lfo.tick(None);

        let leds = lfo.get_leds();
        dbg!(lfo.phasor.val);
        assert_eq!(leds[0], (0., 7));
        assert_eq!(leds[63], (63., 7));
    }

    #[test]
    fn lfo_get_leds_sets_correct_brightness() {
        let mut lfo = Lfo::new(44100, 256);
        lfo.val = 1.;
        let leds = lfo.get_leds();
        assert_eq!(leds[0], (0., 15));
        assert_eq!(leds[32], (32., 15));
        assert_eq!(leds[63], (63., 15));
    }
}
