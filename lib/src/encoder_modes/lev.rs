use crate::encoder_mode::EncoderMode;
use crate::line::Line;
use crate::ring::RingLed;
use crate::shape::Shape;

// A Mode which keeps its value limited to one full rotation, similar to a potentiometer.
// Useful for volume or scrub position.
pub struct Lev {
    pub val: f64,
    unscaled_val: f64,
    line: Line,
    smoothing_time: f64,
    idx: usize,
    pub shape: Shape,
}

impl Iterator for Lev {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        if self.idx > 0 {
            None
        } else {
            self.idx += 1;
            Some(self.val)
        }
    }
}

impl EncoderMode for Lev {
    fn new(sample_rate: usize, _: usize) -> Self {
        Self {
            val: 0.,
            unscaled_val: 0.,
            line: Line::new(sample_rate),
            smoothing_time: 30.,
            idx: 0,
            shape: Shape::default(),
        }
    }

    fn get_input_count(&mut self) -> usize { 0 }
    fn get_output_count(&mut self) -> usize { 1 }
    fn set_block_size(&mut self, _: usize) {}
    fn set_sample_rate(&mut self, new_sr: usize) {
        self.line.set_sample_rate(new_sr);
    }

    fn delta(&mut self, delta: i32) {
        let dest = (self.line.dest + (delta as f64 / 832.))
            .clamp(0., 1.);
        self.line.start(dest, self.smoothing_time);
    }

    fn tick(&mut self, _: Option<std::slice::Iter<Self::Item>>) {
        self.unscaled_val = self.line.tick();
        self.val = self.shape.linear_to_scale(self.unscaled_val);
        self.idx = 0;
    }

    fn get_leds(&mut self) -> Vec<RingLed> {
        const DISP_AREA: f64 = 3. / 4.;

        let bumper_right = (64. * DISP_AREA / 2.).round() + 1.;
        let bumper_left = 64. - bumper_right;

        // scale to the amount of the dial we're using to show it
        let led_pos = self.unscaled_val * DISP_AREA * 64.;
        const ROT_VAL: f64 = 32. * DISP_AREA;

        // finally, display
        let mut leds = vec![(bumper_right, 8), (bumper_left, 8), (led_pos - ROT_VAL, 15)];

        for i in 0..led_pos as usize + 1 {
            // rotate CCW by 180deg and display
            leds.push((i as f64 - ROT_VAL, 15));
        }

        leds
    }
}
