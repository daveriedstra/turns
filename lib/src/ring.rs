use monome::Monome;

// State management for an arc ring
// Enqueue lights with Ring::set(position, intensity) then engage them with Ring::update()
//
// Ring flattens the queue to send only the highest intensities and diffs against the previous
// state to send no more messages than absolutely necessary -- smooth lights with no hiccups.
// As a result, entire states must be sent -- any LED enqueued between calls to Ring::update()
// will be turned off!
//
// This struct should be used outside of DSP loops. Send it position-intensity pairs via message
// passing from the audio thread.

/// RingLed represents an antialiased value which Ring will render.
pub type RingLed = (f64, usize);

/// RingLedAliased is a single LED value to be sent to the arc.
pub type RingLedAliased = (usize, usize);

/// How the value is determined for two LEDs enqueued at the same position
#[derive(Debug, PartialEq)]
pub enum FlattenMode {
    /// Add the values together. Appropriate for groups.
    Sum,
    /// Use the higheset value. Default behaviour.
    Max,
    /// Use the lowest value.
    Min,
}

pub struct Ring {
    queue: Vec<RingLedAliased>,
    state: Vec<RingLedAliased>,
    enc: usize,
    pub flatten_mode: FlattenMode,
    /// Enable debug logging in development builds. Potentially logs every DSP block -- be
    /// judicious.
    pub debug: bool,
}

impl Ring {
    pub fn new(enc: usize) -> Self {
        Self {
            queue: Vec::new(),
            state: Vec::new(),
            enc,
            flatten_mode: FlattenMode::Max,
            debug: false,
        }
    }

    pub fn enqueue(&mut self, led: RingLedAliased) {
        self.queue.push(led);
    }

    pub fn enqueue_all(&mut self, mut map: Vec<RingLedAliased>) {
        self.queue.append(&mut map);
    }

    /// Enqueues the given fractional position-intensity pair as antialiased values.
    pub fn enqueue_aa(&mut self, position: f64, intensity: usize) {
        self.enqueue_all(Vec::from(Ring::antialias(position, intensity)));
    }

    pub fn enqueue_all_aa(&mut self, map: Vec<RingLed>) {
        for led in map {
            self.enqueue_aa(led.0, led.1);
        }
    }

    /// Returns a pair of RingLed representing the antialised position of an input with fractional
    /// position (ie, the position parameter here should be on the scale of 64)
    fn antialias(mut position: f64, intensity: usize) -> [RingLedAliased; 2] {
        while position < 0. {
            position += 64.;
        }
        let pos_aa = position.floor();
        let pos_fract = position.fract();
        [
            (
                pos_aa as usize % 64,
                ((1. - pos_fract) * intensity as f64).round() as usize,
            ),
            (
                (pos_aa as usize + 1) % 64,
                ((pos_fract) * intensity as f64).round() as usize,
            ),
        ]
    }

    pub fn update(&mut self, arc: &mut Monome) {
        self.flatten_queue();
        let delta = self.get_delta();

        // uncomment to log updates
        // if cfg!(debug_assertions) && self.debug && !delta.is_empty() {
        //     dbg!(&self.state);
        //     dbg!(&self.queue);
        //     dbg!(&delta);
        // }

        // if there's a change in the lights, build & send a map
        if !delta.is_empty() {
            let mut map = [0; 64];
            for led in &self.queue {
                map[led.0] = led.1 as u8;
            }
            arc.ring_map(self.enc, &map);
        }

        // replace state
        self.state = self.queue.clone();
        self.queue.clear();
    }

    // Forces the Ring to redraw its state. Useful for things like pagination.
    pub fn refresh(&mut self, arc: &mut Monome) {
        self.queue = self.state.clone();
        self.state.clear();
        self.update(arc);
    }

    // gets the difference between the queue and the state
    fn get_delta(&mut self) -> Vec<RingLedAliased> {
        let mut out = Vec::new();

        for led in &self.queue {
            // only update and don't turn off leds off
            if !self.state.contains(&led) && led.1 != 0 {
                // new intensity -- set
                out.push(*led);
            }
        }

        // only tell Arc to disable lights which are on and shouldn't be
        let queued_indices: Vec<usize> = self.queue.iter().map(|x| x.0).collect();
        for led in &self.state {
            if !queued_indices.contains(&led.0) {
                // expired intensity -- unset
                out.push((led.0, 0));
            }
        }

        out
    }

    fn flatten_queue(&mut self) {
        self.queue.sort_unstable();
        self.queue.reverse();
        match self.flatten_mode {
            FlattenMode::Sum => {
                self.queue.dedup_by(|a, b| {
                    let same_pos = a.0 == b.0;
                    if same_pos {
                        b.1 = (a.1 + b.1).clamp(0, 15);
                    }
                    same_pos
                });
            }
            FlattenMode::Max => {
                self.queue
                    .dedup_by(|(a_pos, a_int), (b_pos, b_int)| (a_pos == b_pos) && (a_int < b_int));
            }
            FlattenMode::Min => {
                self.queue.dedup_by(|a, b| {
                    let same_pos = a.0 == b.0;
                    if same_pos {
                        let lower = a.1.min(b.1);
                        b.1 = lower;
                    }
                    same_pos
                });
            }
        }

        // remove any 0-values in queue - these are implicit but having them can mess up logic
        self.queue = self.queue.drain(..).filter(|&x| x.1 > 0).collect();

        self.queue.reverse();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn enqueue_should_add_to_queue() {
        let mut ring = Ring::new(0);
        ring.enqueue((0, 5));
        assert_eq!(ring.queue, vec![(0, 5)]);
        ring.enqueue((5, 15));
        assert_eq!(ring.queue, vec![(0, 5), (5, 15)]);
    }

    #[test]
    fn flatten_queue_should_flatten() {
        let mut ring = Ring::new(0);
        ring.enqueue((5, 10));
        ring.enqueue((5, 4));
        ring.enqueue((5, 15));
        ring.enqueue((63, 9));
        ring.flatten_queue();
        assert_eq!(ring.queue, vec![(5, 15), (63, 9)]);
    }

    #[test]
    fn flatten_should_use_max_by_default() {
        // testing both behaviour and value
        let mut ring = Ring::new(0);
        ring.enqueue((5, 10));
        ring.enqueue((5, 5));
        ring.flatten_queue();
        assert_eq!(ring.queue, vec![(5, 10)]);
        assert_eq!(ring.flatten_mode, FlattenMode::Max);
    }

    #[test]
    fn flatten_should_use_max_properly() {
        let mut ring = Ring::new(0);
        ring.flatten_mode = FlattenMode::Max;
        ring.enqueue((5, 10));
        ring.enqueue((5, 5));
        ring.flatten_queue();
        assert_eq!(ring.queue, vec![(5, 10)]);
    }

    #[test]
    fn flatten_should_use_min_properly() {
        let mut ring = Ring::new(0);
        ring.flatten_mode = FlattenMode::Min;
        ring.enqueue((5, 10));
        ring.enqueue((5, 5));
        ring.flatten_queue();
        assert_eq!(ring.queue, vec![(5, 5)]);
    }

    #[test]
    fn flatten_should_use_sum_properly() {
        let mut ring = Ring::new(0);
        ring.flatten_mode = FlattenMode::Sum;
        ring.enqueue((5, 10));
        ring.enqueue((5, 5));
        ring.flatten_queue();
        assert_eq!(ring.queue, vec![(5, 15)]);
    }

    #[test]
    fn ring_shouldnt_update_if_not_needed() {
        let mut ring = Ring::new(0);
        ring.state = vec![(5, 10), (10, 3)];
        ring.enqueue((5, 10));
        ring.enqueue((10, 3));
        ring.flatten_queue();
        let delta = ring.get_delta();
        assert_eq!(delta, vec![]);
    }

    #[test]
    fn ring_shouldnt_update_all_if_not_needed() {
        let mut ring = Ring::new(0);
        for i in 0..64 {
            ring.state.push((i, 3));
        }
        for i in 0..64 {
            ring.enqueue((i, 3));
        }
        ring.flatten_queue();
        let delta = ring.get_delta();
        assert_eq!(delta, vec![]);
    }

    #[test]
    fn ring_antialias_should_generate_correct_values() {
        // basic test
        let res = Ring::antialias(3.5, 5);
        assert_eq!(res[0].0, 3);
        assert_eq!(res[1].0, 4);
        assert_eq!(res[0].1, 3);
        assert_eq!(res[1].1, 3);

        // off-centre test
        let res = Ring::antialias(25.2, 15);
        assert_eq!(res[0].0, 25);
        assert_eq!(res[1].0, 26);
        assert_eq!(res[0].1, 12);
        assert_eq!(res[1].1, 3);

        // aliased
        let res = Ring::antialias(63., 1);
        assert_eq!(res[0].0, 63);
        assert_eq!(res[1].0, 0);
        assert_eq!(res[0].1, 1);
        assert_eq!(res[1].1, 0);

        // negative position
        let res = Ring::antialias(-5.4, 10);
        assert_eq!(res[0].0, 58);
        assert_eq!(res[1].0, 59);
        assert_eq!(res[0].1, 4);
        assert_eq!(res[1].1, 6);
    }

    #[test]
    fn ring_antialias_should_give_same_output_as_alias_when_appropriate() {
        let mut ring_0 = Ring::new(0);
        let mut ring_1 = Ring::new(1);
        ring_0.enqueue_all_aa(vec![(0., 15), (1., 15)]);
        ring_0.flatten_queue();
        let delta_0 = ring_0.get_delta();
        ring_1.enqueue_all(vec![(0, 15), (1, 15)]);
        ring_1.flatten_queue();
        let delta_1 = ring_1.get_delta();
        assert_eq!(delta_0, delta_1);
    }
}
