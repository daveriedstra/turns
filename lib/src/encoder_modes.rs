pub mod delta;
pub mod dial;
pub mod dual_lfo;
pub mod lev;
pub mod lfo;
pub mod quad_lfo;
pub mod seq;
pub mod val;
