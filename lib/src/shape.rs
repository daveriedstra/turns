#[derive(Copy, Clone, Debug)]
pub struct Transformation {
    pub offset_x: f64,
    pub offset_y: f64,
    pub scale_x: f64,
    pub scale_y: f64,
}

#[derive(Clone, Debug)]
pub enum Shape {
    /// Mirrors the inner Shape when input is negative (IE, counter-clockwise) so that the negative
    /// output matches the curve of the positive output in the negative direction starting from the
    /// crossover point (ie, avoiding jumps / discontinuities). This can a) be really useful for
    /// simple applications of Log shape; b) do nothing but extra work in Linear shape; and c)
    /// create weird curves in Squared -- but the a) use case makes this worth it.
    /// (Be carfeul to avoid recursion.)
    // MirroredAtZero(Shape),
    MirroredNegative(Box::<Shape>),
    /// f(x) = x
    Linear(Transformation),
    /// f(x) = x²
    Squared(Transformation),
    /// f(x) = log2(x)
    Logarithmic(Transformation),
    /// f(x) = 2ˣ
    Exponential(Transformation),
}

impl Default for Shape {
    fn default() -> Self {
        Shape::Linear(
            Transformation {
                offset_x: 0.,
                offset_y: 0.,
                scale_x: 1.,
                scale_y: 1.,
            }
        )
    }
}

impl Shape {
    /// Scales an input (0-1 based) according to its own algorithm.
    ///
    /// Offset and Transformation are interpreted geometrically. This is easier to understand if you
    /// imagine the input as numbers of rotations of the encoder (a full rotation is 1). This
    /// value is represented along the X (horizontal) axis, and the output value along the Y (vertical) axis.
    /// So, if you want your starting value to be 2 instead of 0, set offset-y = 2. If you want to
    /// have more range in each turn, set scale-y = 2 so that each turn covers twice as much
    /// range in the output.
    ///
    /// A configuration can determine the appropriate values as syntactic sugar for, eg, a Hz offset.
    /// This is included in the turns binary.
    #[inline]
    pub fn linear_to_scale(&self, input: f64) -> f64
    {
        match self {
            Shape::Linear(t) => {
                let output = self.x(input, &t);
                self.y(output, &t)
            },
            Shape::Squared(t) => {
                let output = self.x(input, &t);
                let output = output.powf(2.);
                self.y(output, &t)
            },
            Shape::Logarithmic(t) => {
                let output = self.x(input, &t);
                let output = output.log2();
                self.y(output, &t)
            },
            Shape::Exponential(t) => {
                let output = self.x(input, &t);
                let output = 2_f64.powf(output);
                self.y(output, &t)
            },
            Shape::MirroredNegative(inner_shape) => {
                let output = inner_shape.linear_to_scale(input.abs()) * input.signum();
                if input < 0. {
                    output + 2. * inner_shape.linear_to_scale(0.)
                } else {
                    output
                }
            }
        }
    }

    #[inline]
    fn x(&self, input: f64, t: &Transformation) -> f64 {
        if t.scale_x != 0. {
            (input - t.offset_x) / t.scale_x
        } else {
            0.
        }
    }

    #[inline]
    fn y(&self, input: f64, t: &Transformation) -> f64 {
        input * t.scale_y + t.offset_y
    }
}

mod tests {
    #[allow(unused_imports)]
    #[allow(dead_code)]
    use super::*;

    // Linear tests
    fn test_linear_with_transform(t: Transformation) {
        let shape = Shape::Linear(t);

        // test 0-1
        for i in 0..11 {
            let input = i as f64 / 11.;
            let output = if t.scale_x != 0. {
                (input - t.offset_x) / t.scale_x
            } else { 0. };
            let output = output * t.scale_y + t.offset_y;
            assert_eq!(shape.linear_to_scale(input), output);
        }

        // test 0-101
        for i in 0..10 {
            let input = i as f64 * 101.;
            let output = if t.scale_x != 0. {
                (input - t.offset_x) / t.scale_x
            } else { 0. };
            let output = output * t.scale_y + t.offset_y;
            assert_eq!(shape.linear_to_scale(input), output);
        }
    }

    #[test]
    fn linear_with_default_transform() {
        test_linear_with_transform(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
    }

    #[test]
    fn linear_with_transform_individual_elements() {
        for off in 0..10 {
            // offset_x
            test_linear_with_transform(Transformation {
                offset_x: off as f64,
                offset_y: 0.,
                scale_x: 1.,
                scale_y: 1.,
            });

            // offset_y
            test_linear_with_transform(Transformation {
                offset_x: 0.,
                offset_y: off as f64,
                scale_x: 1.,
                scale_y: 1.,
            });

            // scale_x
            test_linear_with_transform(Transformation {
                offset_x: 0.,
                offset_y: 0.,
                scale_x: off as f64,
                scale_y: 1.,
            });

            // scale_y
            test_linear_with_transform(Transformation {
                offset_x: 0.,
                offset_y: 0.,
                scale_x: 1.,
                scale_y: off as f64,
            });
       }
    }

    #[test]
    fn linear_with_transform_combined_elements() {
        for a in -10..10 {
            for b in -13..13 {
                for c in -9..9 {
                    let scale_y = if c != 0 {
                        a as f64 / c as f64
                    } else {
                        a as f64
                    };

                    test_linear_with_transform(Transformation {
                        offset_x: a as f64,
                        offset_y: b as f64,
                        scale_x: c as f64,
                        scale_y,
                    });
                }
            }
       }
    }

    // we use hardcoded values from here on

    // Squared tests
    #[test]
    fn square() {
        let shape = Shape::Squared(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!(shape.linear_to_scale(2.), 4.);

        let shape = Shape::Squared(Transformation {
            offset_x: 0.,
            offset_y: 1.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!(shape.linear_to_scale(2.), 5.);

        let shape = Shape::Squared(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 2.,
        });
        assert_eq!(shape.linear_to_scale(2.), 8.);

        let shape = Shape::Squared(Transformation {
            offset_x: 1.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!(shape.linear_to_scale(2.), 1.);

        let shape = Shape::Squared(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 2.,
            scale_y: 1.,
        });
        assert_eq!(shape.linear_to_scale(2.), 1.);
    }

    // Logarithmic tests
    #[test]
    fn log() {
        let shape = Shape::Logarithmic(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (1. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (1.32 * 10.) as i32);

        let shape = Shape::Logarithmic(Transformation {
            offset_x: 0.,
            offset_y: 1.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (2. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (2.32 * 10.) as i32);

        let shape = Shape::Logarithmic(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 2.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (2. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (2.64 * 10.) as i32);

        let shape = Shape::Logarithmic(Transformation {
            offset_x: 1.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (0. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (0.58 * 10.) as i32);

        let shape = Shape::Logarithmic(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 2.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (0. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (0.32 * 10.) as i32);
    }


    // Exponential tests
    #[test]
    fn expo() {
        let shape = Shape::Exponential(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (4. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (5.66 * 10.) as i32);

        let shape = Shape::Exponential(Transformation {
            offset_x: 0.,
            offset_y: 1.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (5. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (6.66 * 10.) as i32);

        let shape = Shape::Exponential(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 2.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (8. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (11.31 * 10.) as i32);

        let shape = Shape::Exponential(Transformation {
            offset_x: 1.,
            offset_y: 0.,
            scale_x: 1.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (2. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (2.83 * 10.) as i32);

        let shape = Shape::Exponential(Transformation {
            offset_x: 0.,
            offset_y: 0.,
            scale_x: 2.,
            scale_y: 1.,
        });
        assert_eq!((shape.linear_to_scale(2.) * 10.) as i32, (2. * 10.) as i32);
        assert_eq!((shape.linear_to_scale(2.5) * 10.) as i32, (2.38 * 10.) as i32);
    }
}
