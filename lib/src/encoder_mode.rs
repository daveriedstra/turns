use crate::ring::RingLed;

/// Mode for an Arc encoder. Handles input deltas in delta(), processes optional input values and
/// returns output values from tick(), returns LEDs from get_leds(). Can be run at audio rate or at
/// control rate in its own thread.
///
/// I - number of input streams
/// O - number of output streams
///
pub trait EncoderMode: Iterator {
    /// Instantiate the mode. In control rate, the sample_rate value should be
    /// `1000 / refresh_interval_ms` and block_size should be 1.
    fn new(sample_rate: usize, block_size: usize) -> Self
    where
        Self: Sized;

    /// Handles input events from the Arc.
    fn delta(&mut self, delta: i32);

    /// Updates the current values. In DSP context, this should be run for every
    /// sample. In a control rate context, this should be run every refresh cycle (whether the mode
    /// is on the active page or not).
    ///
    /// Use the EncoderMode's Iterator implementation to access the values.
    fn tick(&mut self, input: Option<std::slice::Iter<Self::Item>>);

    /// Returns an array of LEDs to update the Arc reflective of this Mode's state. In DSP context,
    /// this should be run once every block (and other once-per-block processing can also be handled
    /// in this function). In control context, this should be run for active pages only immediately
    /// after calling tick(). The return value should be passed from the EncoderMode's thread to
    /// the thread communicating with the Arc.
    ///
    /// **TODO:** figure out how to change from Vec to array output in order to avoid allocating on the
    /// heap in audio threads.
    fn get_leds(&mut self) -> Vec<RingLed>;

    fn set_sample_rate(&mut self, _new_sr: usize);
    fn set_block_size(&mut self, _new_bs: usize);

    /// Returns the number of input streams.
    fn get_input_count(&mut self) -> usize;

    /// Returns the number of output streams.
    fn get_output_count(&mut self) -> usize;
}
