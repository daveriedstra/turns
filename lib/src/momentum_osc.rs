use crate::phasor::{get_block_dur, Phasor};
use crate::ring::RingLed;
use crate::wave_shape::WaveShape;

/// Sine Oscillator which can be "thrown" by deltas
pub struct MomentumOsc {
    pub brightness: usize,
    pub phasor: Phasor,
    val: f64,
    phase: f64,

    count: Vec<i32>,
    last_freqs: Vec<f64>,
    block_duration: f64,
    wave_shape: WaveShape,
}

impl MomentumOsc {
    pub fn new(sample_rate: usize, block_size: usize) -> Self {
        MomentumOsc {
            phasor: Phasor::new(sample_rate),
            val: 0.,
            phase: 0.,
            count: Vec::new(),
            last_freqs: Vec::new(),
            brightness: 15,
            block_duration: get_block_dur(sample_rate, block_size),
            wave_shape: WaveShape::Sine,
        }
    }

    pub fn tick(&mut self) -> f64 {
        self.val = self.phasor.tick();
        self.phase = self.phasor.val;
        self.wave_shape.phase_to_wave(self.val)
    }

    pub fn get_led(&mut self) -> RingLed {
        let mut pos = (self.phase * 64.).round();
        while pos < 0. {
            pos += 64.;
        }
        (pos, self.brightness)
    }

    pub fn get_antialiased_leds(&mut self) -> RingLed {
        let mut pos = self.phase * 64.;
        while pos < 0. {
            pos += 64.;
        }
        (pos, self.brightness)
    }

    pub fn new_delta(&mut self, delta: i32) {
        self.count.push(delta);
    }

    /**
     * Updates the oscillator frequency, to be called regularly (eg, every block).
     * Tallies all the deltas which have been added since last call, divides them by the duration
     * between calls (tally_dur argument) to produce a frequency. This value is not used directly,
     * but added to last_freqs, which is averaged and kept under 10 items.
     */
    pub fn update_freq(&mut self) {
        if !self.count.is_empty() {
            // This used to divide by block_duration, but somehow changing it to be
            // independent makes things _more_ consistent across block & sample rates,
            // so now it divides by roughly 256 / 44100 and it's pretty good but not perfect.
            let mut freq = self.count.iter().sum::<i32>() as f64 / 0.005805;

            let sign = freq.signum();
            freq -= sign; // move closer to 0
            freq = (freq.abs() / 2048.).powf(1.8) * sign; // sensitivity & curve

            // use avg of last 10 freqs
            self.last_freqs.push(freq);
            if self.last_freqs.len() > 10 {
                self.last_freqs.remove(0);
            }
            freq = self.last_freqs.iter().sum::<f64>() / self.last_freqs.len() as f64;

            self.phasor.set_freq_ramp(freq, self.block_duration);
            self.count.clear();
        }
    }

    pub fn set_wave_shape(&mut self, wave_shape: WaveShape) {
        self.wave_shape = wave_shape;
    }

    pub fn set_block_duration(&mut self, new_dur: f64) {
        self.block_duration = new_dur;
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    // you'd be surprised...
    #[test]
    fn momentum_should_not_be_inf() {
        let block_size = 256;
        let mut mosc = MomentumOsc::new(44100, block_size);

        // simulate 5 audio blocks
        for i in 0..5 {
            for _ in 0..5 {
                mosc.new_delta(i + 1);
            }
            mosc.update_freq();

            let f = mosc.phasor.get_freq();
            assert!(f.is_finite());
            assert!(f >= f64::EPSILON);

            // shouldn't have any effect, but to be thorough...
            for _ in 0..block_size {
                mosc.tick();
            }
        }
    }

    // There were some tests here to ensure momentum stayed the same across sample rates and block
    // sizes. When these tests were written, momentum behaved differently across SR & BS changes;
    // fixing those behaviours empirically broke the tests. (The tests were removed 2022-01-15, if
    // you're hunting down a commit to find them.)
}
