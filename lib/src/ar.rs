use crate::line::Line;

pub struct Ar {
    line: Line,
    attacking: bool,
    release_ms: Option<f64>,
}

impl Iterator for Ar {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        Some(self.tick())
    }
}

impl Ar {
    pub fn new(sample_rate: usize) -> Self {
        Ar {
            line: Line::new(sample_rate),
            attacking: false,
            release_ms: None,
        }
    }

    pub fn tick(&mut self) -> f64 {
        if self.attacking && self.line.get_progress() == 1. {
            self.line.start(0., self.release_ms.unwrap_or(0.));
            self.attacking = false;
        }
        self.line.tick()
    }

    pub fn start(&mut self, attack_ms: f64, release_ms: f64) {
        self.line.start(1., attack_ms);
        self.attacking = true;
        self.release_ms = Some(release_ms);
    }

    pub fn set_sample_rate(&mut self, new_sr: usize) {
        self.line.set_sample_rate(new_sr);
    }
}
