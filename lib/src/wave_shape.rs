#[derive(Copy, Clone, Debug)]
pub enum WaveShape {
    Square,
    Triangle,
    Sine,
    SawRising,
    SawFalling,
}

impl WaveShape {
    /// Converts a phase (0-1) to a wave (0-1) of its own shape
    #[inline]
    pub fn phase_to_wave(&self, phase: f64) -> f64
    {
        match self {
            WaveShape::Square => {
                if phase > 0.5 { 1. } else { 0. }
            },
            WaveShape::Triangle => {
                (phase * 2. - 1.).abs()
            },
            WaveShape::Sine => {
                (phase * std::f64::consts::TAU).sin() / 2. + 0.5
            },
            WaveShape::SawRising => {
                phase
            },
            WaveShape::SawFalling => {
                1. - phase
            },
        }
    }
}
