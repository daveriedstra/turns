pub struct Line {
    val: f64,
    origin: f64,
    pub dest: f64,
    increment: f64,
    increasing: bool,
    sample_rate: usize,
}

impl Iterator for Line {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        Some(self.tick())
    }
}

impl Line {
    pub fn new(sample_rate: usize) -> Self {
        Line {
            val: 0.,
            origin: 0.,
            dest: 0.,
            increment: 0.,
            increasing: false,
            sample_rate,
        }
    }

    // add increment to value only if needed
    // ensure we reach value if last step would result in overshoot
    pub fn tick(&mut self) -> f64 {
        if (self.val - self.dest).abs() > f64::EPSILON {
            self.val += self.increment;

            if self.increasing {
                self.val = self.val.min(self.dest);
            } else {
                self.val = self.val.max(self.dest);
            }
        }

        self.val
    }

    // calculate a new line
    // can be started in the midst of an ongoing one
    // reset origin
    // determine direction (self.increasing)
    // determine increment
    pub fn start(&mut self, dest: f64, duration_ms: f64) {
        self.origin = self.val;
        self.dest = dest;

        let difference = dest - self.val;
        self.increasing = difference.signum() as usize == 1;

        let sample_dur = 1000. / self.sample_rate as f64;
        let num_steps = duration_ms / sample_dur;
        self.increment = difference / num_steps;
    }

    /**
     * Returns the progress of the line towards its destination (0-1)
     */
    pub fn get_progress(&mut self) -> f64 {
        let denom = (self.dest - self.origin).abs();
        if denom < f64::EPSILON {
            1.
        } else {
            (self.val - self.origin).abs() / denom
        }
    }

    pub fn set_sample_rate(&mut self, new_sr: usize) {
        self.sample_rate = new_sr;
    }
}

mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn line_creates_ramp_to_dest() {
        let sr = 4;
        let mut line = Line::new(sr);
        line.start(1., 1000.);

        line.tick();
        assert_eq!(line.val, 0.25);
        line.tick();
        assert_eq!(line.val, 0.50);
        line.tick();
        assert_eq!(line.val, 0.75);
        line.tick();
        assert_eq!(line.val, 1.);
    }

    #[test]
    fn line_reaches_destination_on_time() {
        let sr = 441;
        let mut line = Line::new(sr);

        let dest = 1.;
        let dur = 1000.;
        let samps = sr as f64 / dur * 1000.;

        line.start(dest, dur);
        for _ in 0..samps as usize {
            line.tick();
        }
        assert_eq!(line.val, dest);

        let dest = -0.1234;
        let dur = 132.;
        let samps = sr as f64 / dur * 1000.;

        line.start(dest, dur);
        for _ in 0..samps.ceil() as usize {
            line.tick();
        }
        assert_eq!(line.val, dest);
    }

    #[test]
    fn line_can_be_interrupted() {
        let mut line = Line::new(480);

        // start a line to 1
        line.start(1., 1000.);
        for _ in 0..240 {
            line.tick();
        }
        assert_ne!(line.val, 1.);

        // interrupt halfway by starting a line to 2.
        line.start(2., 1000.);
        for _ in 0..480 {
            line.tick();
        }

        // "1 second" after interrupting we should be at two
        assert_eq!(line.val, 2.);
    }

    #[test]
    fn line_works_with_weird_floats() {
        let mut line = Line::new(480);
        let dest = 0.1 + 0.2;

        line.start(dest, 200.);
        for _ in 0..480 {
            line.tick();
        }
        assert_eq!(line.val, dest);
    }
}
