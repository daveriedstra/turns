use crate::line::Line;

#[cfg(feature = "dasp_oscs")]
use dasp::signal::{self, Signal, Rate, Phase, Hz, from_iter, FromIterator};

/**
 * Sawtooth oscillator from 0-1
 */
#[cfg(not(feature = "dasp_oscs"))]
pub struct Phasor {
    pub val: f64,
    pub interval: f64,
    freq: f64,
    freq_line: Line,
    sample_rate: usize,
}

#[cfg(feature = "dasp_oscs")]
pub struct Phasor {
    pub val: f64,
    pub interval: f64,
    rate: Rate,
    phase: Phase<Hz<FromIterator<Line>>>,
    freq: f64,
    sample_rate: usize,
}

impl Phasor {
    pub fn new(sample_rate: usize) -> Self {
        #[cfg(not(feature = "dasp_oscs"))] {
            Self {
                val: 0.,
                interval: 0.,
                freq: 0.,
                freq_line: Line::new(sample_rate),
                sample_rate,
            }
        }

        #[cfg(feature = "dasp_oscs")] {
            let rate = signal::rate(sample_rate as f64);
            Self {
                val: 0.,
                interval: 0.,
                rate,
                phase: rate.clone().hz(from_iter(Line::new(sample_rate))).phase(),
                freq: 0.,
                sample_rate,
            }
        }

    }

    /**
     * Call once every sample
     */
    pub fn tick(&mut self) -> f64 {
        #[cfg(not(feature = "dasp_oscs"))] {
            self.freq = self.freq_line.tick();
            self.interval = self.freq / self.sample_rate as f64;
            self.val = (1. + self.val + self.interval) % 1.;
        }

        #[cfg(feature = "dasp_oscs")] {
            let val = self.phase.next();
            self.interval = val - self.val;
            self.val = val;
        }

        self.val
    }

    /**
     * Set the frequency of the oscillator immediately
     */
    pub fn set_freq(&mut self, freq: f64) {
        self.set_freq_ramp(freq, 0.);
    }

    /**
     * Sets the freq with interpolation
     */
    pub fn set_freq_ramp(&mut self, freq: f64, ramp_ms: f64) {
        #[cfg(not(feature = "dasp_oscs"))] {
            self.freq_line.start(freq, ramp_ms);
        }

        // NOTE: dasp method (resets phase)
        // TODO: find a way to change frequency with dasp that doesn't reset phase
        #[cfg(feature = "dasp_oscs")] {
            let mut f_line = Line::new(self.sample_rate);
            f_line.start(freq, ramp_ms);
            self.phase = self.rate.hz(from_iter(f_line)).phase();
        }

        // it would be great to have some way of reporting these accurately during the slew, but for
        // now this is our best bet, I think...
        self.freq = freq;
    }

    /**
     * Get the oscillator's frequency
     */
    pub fn get_freq(&mut self) -> f64 {
        self.freq
    }

    /**
     * Set the sample rate
     */
    pub fn set_sample_rate(&mut self, sr: usize) {
        self.sample_rate = sr;

        #[cfg(not(feature = "dasp_oscs"))]
        self.freq_line.set_sample_rate(sr);

        let f = self.get_freq();
        self.set_freq(f);
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn frequency_should_be_correct_at_different_sample_rates() {
        let freqs = [-2., -1., -0.5, -0.25, 0.25, 0.5, 1., 2., 5., 10., 220.];
        let rates = [10, 20, 44100, 48000, 88200, 96000];

        for sr in rates.iter() {
            let freqs_below_nyquist = freqs.iter()
                .filter(|&freq| (*freq) < (sr / 2) as f64);
            for freq in freqs_below_nyquist {
                // Higher frequencies at higher rates are sometimes off by a few frames -- not sure
                // why, but it's close enough to be acceptable to me. I give these a +/- tolerance
                // of three frames if they're a "real" sample rate.
                // let tolerance = if *sr > 1000 { 3. } else { 1. };
                //
                // The dasp oscillators somehow need a little more tolerance (somehow), but they're
                // probably still within the acceptable range.
                let tolerance = 2.;

                let mut p = Phasor::new(*sr);
                p.set_freq(*freq);

                assert!(p.val == 0.);

                let n_cycles = (*sr as f64 / *freq as f64).abs().floor() as usize;
                for _ in 0..n_cycles / 2 {
                    p.tick();
                }

                let abs_diff = (p.val - 0.5).abs();
                let in_range = abs_diff < p.interval.abs() * tolerance;
                if !in_range {
                    let n_frames_deviated = abs_diff / p.interval;
                    dbg!(freq, sr, p.val, p.interval, abs_diff, n_frames_deviated);
                }
                assert!(in_range);

                for _ in 0..n_cycles / 2 {
                    p.tick();
                }

                let abs_diff = (p.val - 1.).abs();
                let in_range = abs_diff.min(p.val) < tolerance * p.interval.abs();
                if !in_range {
                    let n_frames_deviated = abs_diff.min(p.val) / p.interval;
                    dbg!(freq, sr, p.val, p.interval, abs_diff, n_frames_deviated);
                }
                assert!(in_range);
            }
        }
    }

    #[test]
    fn changing_frequency_should_not_affect_phase() {
        let mut p = Phasor::new(4);
        p.set_freq(1.);

        // 1. oscillate to a quarter phase
        p.tick();
        #[cfg(feature = "dasp_oscs")] {
            // dasp outputs a zero sample first
            p.tick();
        }

        // 2. check phase == 0.25
        assert_eq!(p.val, 0.25);

        // 3. assign new freq with 0 slew
        p.set_freq(0.5);

        // 4. oscillate to 50% phase
        p.tick();
        p.tick();
        #[cfg(feature = "dasp_oscs")] {
            p.tick();
        }

        // 5. check phase == 0.5
        assert_eq!(p.val, 0.5);
    }
}

/// Calculates the block duration in seconds using sample rate and block size
pub fn get_block_dur(sample_rate: usize, block_size: usize) -> f64 {
    block_size as f64 / sample_rate as f64
}
