/**
 * outputs true on intersection
 */
pub struct Intersect<const N: usize> {
    i_vec: Vec<[i32;2]>,
    // i_vec: [[i32;2]; N * (N - 1) / 2 + 1], // to use an array instead; requires
                                              // feature(generic_const_exprs)
}

impl <const N: usize> Intersect<N> {
    pub fn new() -> Self {
        let n_comp = N * (N-1) / 2;
        Intersect {
            i_vec: vec![[0,0]; n_comp + 1],
        }
    }

    pub fn tick(&mut self, input_samps: [f64; N]) -> bool
    {
        // compare all inputs:
        // compare i against (i+j+1) and store in k
        let mut k = 0;
        for i in 0..(N - 1) {
            for j in 0..(N - i - 1) {
                self.i_vec[k][0] = (input_samps[i] >= input_samps[i + j + 1]) as i32;
                k += 1;
            }
        }

        // XOR each input comparison against its one-sample delay and check if any are true
        let has_crossing: i32 = self.i_vec.iter()
            .fold(0, |acc, i_vec| {
                let o = acc | (i_vec[0] ^ i_vec[1]);
                o
            });

        // apply one-sample delay
        self.i_vec.iter_mut()
            .for_each(|v| v[1] = v[0]);

        return has_crossing > 0;
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn two_channels_intersect_should_work() {
        let mut intersect = Intersect::<2>::new();

        let frame = [0., 0.1];
        assert!(!intersect.tick(frame));

        // Cross
        let frame = [0.1, 0.];
        assert!(intersect.tick(frame));

        let frame = [0.2, -0.1];
        assert!(!intersect.tick(frame));

        // Cross
        let frame = [0., 0.1];
        assert!(intersect.tick(frame));

        let frame = [-0.2, 0.2];
        assert!(!intersect.tick(frame));
    }

    #[test]
    fn two_channels_intersect_should_work_reverse() {
        let mut intersect = Intersect::<2>::new();

        let frame = [0.1, 0.];
        intersect.tick(frame); // will be true coming from [0., 0.]

        // Cross
        let frame = [0., 0.1];
        assert!(intersect.tick(frame));

        let frame = [-0.1, 0.2];
        assert!(!intersect.tick(frame));
    }

    #[test]
    fn three_channels_intersect_should_work() {
        let mut intersect = Intersect::<3>::new();

        // no cross
        let frame = [-0.1, 0., 0.1];
        assert!(!intersect.tick(frame));

        // A & B cross
        let frame = [0., -0.1, 0.1];
        assert!(intersect.tick(frame));

        // no cross
        let frame = [0.1, -0.2, 0.2];
        assert!(!intersect.tick(frame));

        // A & C cross
        let frame = [0.2, -0.1, 0.1];
        assert!(intersect.tick(frame));

        // no cross
        let frame = [0.3, 0., 0.1];
        assert!(!intersect.tick(frame));

        // B & C cross
        let frame = [0.4, 0.1, 0.];
        assert!(intersect.tick(frame));
    }

    // Error case discovered manually
    #[test]
    fn last_two_of_four_channels_intersect_should_work() {
        let mut intersect = Intersect::<4>::new();
        let mut x;

        // set first two channels to -1 so we don't get false positives
        intersect.tick([-1., -1.1, 0., 0.1]);

        x = intersect.tick([-1., -1.1, 0.1, 0.]);
        assert!(x);

        x = intersect.tick([-1., -1.1, 0.2, -0.1]);
        assert!(!x);
    }
}
