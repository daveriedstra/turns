#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]

extern crate ctrlc;
extern crate monome;
use monome::{Monome, MonomeEvent};

use crossbeam::channel;
use crossbeam::channel::{Receiver, Sender};
use std::error::Error;
use std::{thread, time};

use turns_lib::encoder_modes::dial::Dial;
use turns_lib::encoder_modes::lev::Lev;
use turns_lib::encoder_modes::lfo::Lfo;
use turns_lib::encoder_modes::seq::Seq;
use turns_lib::encoder_modes::val::Val;
use turns_lib::encoder_mode::EncoderMode;
use turns_lib::ring::Ring;
use turns_lib::shape::{Shape, Transformation};

fn main() -> Result<(), Box<dyn Error>> {
    // handle sigint
    let (tx, rx) = channel::bounded(10);
    ctrlc::set_handler(move || {
        tx.send(()).unwrap();
    })?;

    let mut arc = Monome::new("/darc".to_string())?;
    for i in 0..4 {
        arc.ring_all(i, 0);
    }

    const REFRESH_IVAL_MS: usize = 100;
    let sr = 1000 / REFRESH_IVAL_MS;

    let mut enc_0 = Seq::new(sr, 1);
    enc_0.add_stop();
    let enc_1 = Lfo::new(sr, 1);
    let mut enc_2 = Lev::new(sr, 1);
    enc_2.shape = Shape::Linear(Transformation {
        offset_x: 0.,
        offset_y: -5.,
        scale_x: 0.,
        scale_y: 10.
    });
    let mut enc_3 = Dial::new(sr, 1);
    enc_3.set_num_stops(7);

    // This only works because each of these modes has the same number of inputs and outputs
    // Using them as trait objects also means we're limited to the interface described by the
    // trait, so the only way we can programmatically talk to the modes is through tick() (which is
    // why we set enc_2 min and max above.)
    // To cover all the modes regardless of <I, O> you can use enums.
    let mut encoders: Vec<Box<dyn EncoderMode<Item=f64>>> = vec![
        Box::new(enc_0),
        Box::new(enc_1),
        Box::new(enc_2),
        Box::new(enc_3),
    ];

    let mut rings = vec![Ring::new(0), Ring::new(1), Ring::new(2), Ring::new(3)];

    loop {
        // main loop with timeout
        loop {
            // catch all poll events
            match arc.poll() {
                Some(MonomeEvent::EncoderDelta { n, delta }) => {
                    encoders[n.clamp(0, 3)].delta(delta);
                }
                _ => break,
            }
        }

        for (i, e) in encoders.iter_mut().enumerate() {
            e.tick(None);
            rings[i].enqueue_all_aa(e.get_leds());
            rings[i].update(&mut arc);
            // here is where we'd send the value out to MIDI, OSC, or some other protocol
            // let val = e.next();
        }

        // if we get sigint, turn off the lights and exit, otherwise continue
        if let Ok(()) = rx.try_recv() {
            for i in 0..4 {
                arc.ring_all(i, 0);
            }
            thread::sleep(time::Duration::from_millis(100));
            return Ok(());
        } else {
            thread::sleep(time::Duration::from_millis(10));
        }
    }
}
