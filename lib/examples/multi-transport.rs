#![allow(unused_imports)]

extern crate ctrlc;
extern crate helgoboss_midi;
extern crate midir;
extern crate monome;
extern crate rosc;

use monome::{Monome, MonomeEvent};

use helgoboss_midi::{Channel, ControllerNumber, controller_numbers, RawShortMessage,
    ShortMessage, ShortMessageFactory, U7};
use midir::MidiOutput;

use rosc::encoder;
use rosc::OscPacket;
use std::net::UdpSocket;
use std::str::FromStr;

use crossbeam::channel;
use crossbeam::channel::Sender;
use std::error::Error;
use std::{thread, time};
use std::convert::TryInto;

use turns_lib::encoder_modes::{dial::Dial, dual_lfo::DualLfo, lev::Lev, lfo::Lfo, quad_lfo::QuadLfo,
    seq::Seq, val::Val};
use turns_lib::encoder_mode::EncoderMode;
use turns_lib::ring::Ring;
use turns_lib::wave_shape::WaveShape;

const OSC_SENDER_ADDR: &'static str = "127.0.0.1:8080";
const OSC_RECEIVER_ADDR: &'static str = "127.0.0.1:5000";

fn main() -> Result<(), Box<dyn Error>> {
    // handle sigint
    let (tx, rx) = channel::bounded(10);
    ctrlc::set_handler(move || {
        tx.send(()).unwrap();
    })?;

    // start thread to handle transport
    let (midi_tx, osc_tx) = start_transport_thread();

    // get arc handle and configure
    let mut arc = Monome::new("/darc".to_string())?;
    for i in 0..4 {
        arc.ring_all(i, 0);
    }

    const REFRESH_IVAL_MS: usize = 100;
    let sr = 1000 / REFRESH_IVAL_MS;

    let enc_0 = Lev::new(sr, 1);
    let mut enc_1 = Lfo::new(sr, 1);
    enc_1.set_wave_shape(WaveShape::SawRising);
    let enc_2 = Dial::new(sr, 1);
    let enc_3 = DualLfo::new(sr, 1);

    // channels are 0-based, so providing 0 here is actually MIDI channel 1
    let midi_cc_1 = send_midi_cc(0, controller_numbers::CHANNEL_VOLUME, &midi_tx);
    let midi_cc_2 = send_midi_cc(0, controller_numbers::EXPRESSION_CONTROLLER, &midi_tx);

    let osc_vol = send_osc_to("/darc/vol".to_string(), &osc_tx);
    let osc_lfo = (send_osc_to("/darc/lfo/0".to_string(), &osc_tx), send_osc_to("/darc/lfo/1".to_string(), &osc_tx));

    let mut kr_senders = vec![
        KrModeWithTransport::new(Box::new(enc_0), vec![midi_cc_1]),
        KrModeWithTransport::new(Box::new(enc_1), vec![midi_cc_2]),
        KrModeWithTransport::new(Box::new(enc_2), vec![osc_vol]),
        KrModeWithTransport::new(Box::new(enc_3), vec![osc_lfo.0, osc_lfo.1]),
    ];

    let mut rings = vec![Ring::new(0), Ring::new(1), Ring::new(2), Ring::new(3)];

    loop {      // main loop with timeout
        loop {  // catch all poll events
            match arc.poll() {
                Some(MonomeEvent::EncoderDelta { n, delta }) => {
                    kr_senders[n.clamp(0, 3)].mode.delta(delta);
                }
                _ => break,
            }
        }

        // here is where we send out to MIDI, OSC, or some other protocol
        for (i, e) in kr_senders.iter_mut().enumerate() {
            e.update(); // update() calls EncoderMode.tick()
            rings[i].enqueue_all_aa(e.mode.get_leds());
            rings[i].update(&mut arc);
        }

        // if we get sigint, turn off the lights and exit, otherwise continue
        if let Ok(()) = rx.try_recv() {
            for i in 0..4 {
                arc.ring_all(i, 0);
            }
            thread::sleep(time::Duration::from_millis(100));
            return Ok(());
        } else {
            thread::sleep(time::Duration::from_millis(10));
        }
    }
}

// the thread that will handle sending our MIDI & OSC messages. Similar principle could be used for
// sending audio or CV (or whatever else you use your arc for).
fn start_transport_thread() -> (Sender<RawShortMessage>, Sender<rosc::OscMessage>) {
    let (midi_tx, midi_rx) = channel::bounded::<RawShortMessage>(100);
    let (osc_tx, osc_rx) = channel::bounded::<rosc::OscMessage>(100);

    // Choose the first available midi output port and assume it works (do better in your own app!)
    let midi_out = MidiOutput::new("turns test device").unwrap();
    let out_port = &midi_out.ports()[0];
    let mut conn_out = midi_out.connect(out_port, "darc").unwrap();

    // Try to bind to the specified OSC IP address and port -- if it fails, panic! at the disco
    let sender_addr = std::net::SocketAddrV4::from_str(OSC_SENDER_ADDR).unwrap();
    let recv_addr = std::net::SocketAddrV4::from_str(OSC_RECEIVER_ADDR).unwrap();
    let osc_sock = UdpSocket::bind(&sender_addr);

    std::thread::spawn(move || {
        loop {
            // send MIDI
            while let Ok(midi_msg) = midi_rx.try_recv() {
                let msg = [midi_msg.status_byte(), midi_msg.data_byte_1().get(), midi_msg.data_byte_2().get()];
                conn_out.send(&msg).unwrap();
            }

            // send OSC
            if let Ok(ref socket) = osc_sock {
                while let Ok(osc_msg) = osc_rx.try_recv() {
                    // dbg!(&osc_msg);
                    let packet = OscPacket::Message(osc_msg);
                    let msg_buf = encoder::encode(&packet).unwrap();
                    socket.send_to(&msg_buf, &recv_addr).unwrap();
                }
            }

            thread::sleep(time::Duration::from_millis(100));
        }
    });
    
    return (midi_tx, osc_tx);
}

/**
 * This struct associates an EncoderMode, a set of function closures responsible for sending its
 * data to control-rate protocols, and some state to determine whether new values should be sent.
 */
pub struct KrModeWithTransport<'a> {
    pub mode: Box<dyn EncoderMode<Item=f64> + Send>,
    pub do_send: Vec<Box<dyn Fn(f64) + 'a>>,
    pub prev: Vec<f64>,
}

impl<'a> KrModeWithTransport<'a> {
    pub fn new(mode: Box<dyn EncoderMode<Item=f64> + Send>, do_send: Vec<Box<dyn Fn(f64) + 'a>>) -> Self {
        let num_channels = do_send.len();
        KrModeWithTransport {
            mode,
            do_send,
            prev: (0..num_channels).map(|_| 0.).collect(),
        }
    }

    pub fn update(&mut self) {
        self.mode.tick(None);

        // Send each of the output values to its associated lambda if it's been updated.
        let mut i = 0;
        while let Some(next) = self.mode.next() {
            let prev = self.prev[i];

            // call the associated closure for this value
            if (prev - next).abs() > f64::EPSILON {
                (self.do_send[i])(next);
            }

            self.prev[i] = next;
            i += 1;
        }
    }
}


/**
 * A factory to make closures to send MIDI CC messages to the provided crossbeam channel
 */
pub fn send_midi_cc(ch: u8, cc_number: ControllerNumber, midi_tx: &'_ Sender<RawShortMessage>)
    -> Box<dyn Fn(f64) + '_>
{
    Box::new(move |samp: f64| {
        let msg = RawShortMessage::control_change(
            Channel::new(ch),
            cc_number,
            U7::new((samp * 127.).clamp(0., 127.) as u8)
        );

        if let Err(e) = midi_tx.send(msg) {
            dbg!(e);
        };
    })
}

/**
 * A factory for making closures to send OSC messages to the provided crossbeam channel
 */
pub fn send_osc_to(addr: String, osc_tx: &'_ Sender<rosc::OscMessage>)
    -> Box<dyn Fn(f64) + '_>
{
    Box::new(move |samp: f64| {
        let msg = rosc::OscMessage {
            addr: addr.to_owned(),
            args: vec!(rosc::OscType::Float(samp as f32)),
        };

        if let Err(e) = osc_tx.send(msg) {
            dbg!(e);
        };
    })
}
