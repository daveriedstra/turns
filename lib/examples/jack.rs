extern crate monome;
use monome::{Monome, MonomeEvent};

extern crate ctrlc;

use turns_lib::encoder_modes::{
    dual_lfo::DualLfo, lfo::Lfo, quad_lfo::QuadLfo, val::Val,
};
use turns_lib::encoder_mode::EncoderMode;
use turns_lib::ring::{Ring, RingLed};

use crossbeam::channel;
use crossbeam::channel::{Receiver, Sender};

enum DspMsg {
    Quit,
    Delta { n: usize, delta: i32 },
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Testing monome arc with jackd outputs...");

    // start jack client thread
    // start_jack_client() returns crossbeam channels which we'll use to communicate between the
    // audio thread and the control thread, as well as a reference to the client (which we need to
    // hold on to, otherwise it will disappear immediately).
    println!("Starting jack client...");
    let (to_dsp, from_dsp, _client) = start_jack_client()?;

    // the arc polling thread is the main thread
    // we only ever poll the arc and set its lights here
    println!("Starting arc thread...");
    let mut arc = Monome::new("/darc".to_string())?;
    for i in 0..4 {
        arc.ring_all(i, 0);
    }

    // set up sigint handler
    let (to_main, from_ctrlc) = channel::bounded(10);
    ctrlc::set_handler(move || {
        to_main.send(()).unwrap();
    })?;

    let mut rings: [Ring; 4] = [Ring::new(0), Ring::new(1), Ring::new(2), Ring::new(3)];

    // Some of the encoder modes look better with different flattening modes.
    // Sum will enable Val's position indicator to be visible even if it exactly overlays a tally
    // indicator.
    rings[2].flatten_mode = turns_lib::ring::FlattenMode::Sum;

    loop {
        // poll arc for values
        loop {
            match arc.poll() {
                Some(MonomeEvent::EncoderDelta { n, delta }) => {
                    to_dsp.send(DspMsg::Delta { n, delta })?;
                }
                _ => break,
            }
        }

        // poll receiver for light updates
        while let Ok((enc, leds)) = from_dsp.try_recv() {
            // update rings
            rings[enc].enqueue_all_aa(leds);
            rings[enc].update(&mut arc);
        }

        // handle SIGINT
        if let Ok(()) = from_ctrlc.try_recv() {
            for i in 0..4 {
                arc.ring_all(i, 0);
            }
            std::thread::sleep(std::time::Duration::from_millis(100));
            to_dsp.send(DspMsg::Quit).unwrap();
            return Ok(());
        }

        std::thread::sleep(std::time::Duration::from_millis(10));
    }
}

/**
 * Configure and run the DSP.
 */
fn start_jack_client() -> Result<
    (
        Sender<DspMsg>,
        Receiver<(usize, Vec<RingLed>)>,
        Box<dyn std::any::Any>,
    ),
    jack::Error,
> {
    let (client, _) = jack::Client::new("darcrs", jack::ClientOptions::NO_START_SERVER)?;
    let sample_rate = client.sample_rate();
    let buffer_size = client.buffer_size() as usize;
    let spec = jack::AudioOut::default();

    // define output channels
    let mut lfo_out_1 = client.register_port("lfo_1", spec)?;
    let mut dual_lfo_out_2 = client.register_port("dual_lfo_2", spec)?;
    let mut dual_lfo_out_3 = client.register_port("dual_lfo_3", spec)?;
    let mut val_out_0 = client.register_port("val_0", spec)?;
    let mut quad_lfo_out_0 = client.register_port("quad_lfo_0", spec)?;
    let mut quad_lfo_out_1 = client.register_port("quad_lfo_1", spec)?;
    let mut quad_lfo_out_2 = client.register_port("quad_lfo_2", spec)?;
    let mut quad_lfo_out_3 = client.register_port("quad_lfo_3", spec)?;
    let mut quad_lfo_out_trig = client.register_port("quad_lfo_trig", spec)?;

    // define input channels
    // try patching one of the other LFOs to this port
    let quad_lfo_in = client.register_port("quad_lfo_freq", jack::AudioIn::default())?;

    // start up our encoder modes
    let mut enc_0 = Lfo::new(sample_rate, buffer_size);
    let mut enc_1 = DualLfo::new(sample_rate, buffer_size);
    let mut enc_2 = Val::new(sample_rate, buffer_size);
    let mut enc_3 = QuadLfo::new(sample_rate, buffer_size);

    // set starting multiplier for QuadLfo
    enc_3.multiplier = 0.5;

    // engage our two-way communication
    let (delta_tx, delta_rx) = channel::bounded(100);
    let (ring_tx, ring_rx) = channel::bounded(100);

    let dsp_cb = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        // get output buffers
        let lfo_buff = lfo_out_1.as_mut_slice(ps);
        let dlfo_buff_0 = dual_lfo_out_2.as_mut_slice(ps);
        let dlfo_buff_1 = dual_lfo_out_3.as_mut_slice(ps);
        let val_buff = val_out_0.as_mut_slice(ps);
        let qlfo_buff_0 = quad_lfo_out_0.as_mut_slice(ps);
        let qlfo_buff_1 = quad_lfo_out_1.as_mut_slice(ps);
        let qlfo_buff_2 = quad_lfo_out_2.as_mut_slice(ps);
        let qlfo_buff_3 = quad_lfo_out_3.as_mut_slice(ps);
        let qlfo_buff_trig = quad_lfo_out_trig.as_mut_slice(ps);

        // get input buffers
        let qlfo_input = quad_lfo_in.as_slice(ps);

        // get delta values from arc
        while let Ok(msg) = delta_rx.try_recv() {
            match msg {
                DspMsg::Quit => {
                    return jack::Control::Quit;
                }
                DspMsg::Delta { n, delta } => {
                    // encoders[n].delta(delta); // if all your encoders use the same number of input &
                    // output channels, you could address them in an array and still keep them off the
                    // heap (see "modes" example); otherwise, we use pattern matching:
                    match n {
                        0 => enc_0.delta(delta),
                        1 => enc_1.delta(delta),
                        2 => enc_2.delta(delta),
                        3 => enc_3.delta(delta),
                        _ => {}
                    }
                }
            }
        }

        // fill output buffers
        // Encoder 0 - LFO
        // tick() takes an array of input samples and returns an array of output samples.
        // since this mode doens't use input, we pass an empty array [].
        for v in lfo_buff.iter_mut() {
            enc_0.tick(None);
            *v = enc_0.next().unwrap_or(0.) as f32;
        }

        // Encoder 1 - Dual LFO
        // iterate through both channels in one loop
        let mut v1 = dlfo_buff_1.iter_mut();
        for v0 in dlfo_buff_0.iter_mut() {
            enc_1.tick(None);
            *v0 = enc_1.next().unwrap_or(0.) as f32;
            *v1.next().unwrap() = enc_1.next().unwrap_or(0.) as f32;
        }

        // Encoder 2 - Value and Encoder 3 - Quad Lfo
        // Encoder 2's output affects Encoder 3 at audio rate, so we process all
        // their audio streams in the same loop
        let mut v0 = qlfo_buff_0.iter_mut();
        let mut v1 = qlfo_buff_1.iter_mut();
        let mut v2 = qlfo_buff_2.iter_mut();
        let mut v3 = qlfo_buff_3.iter_mut();
        let mut v_trig = qlfo_buff_trig.iter_mut();
        let mut i = qlfo_input.iter();
        for val_samp in val_buff.iter_mut() {
            enc_2.tick(None);
            *val_samp = enc_2.next().unwrap_or(0.) as f32;

            // audio input from jack is summed with enc_2's value and sent to QuadLfo's
            // input via tick. We could alternatively write this to enc_3.multiplier,
            // but that would override our initial setting.
            let qlfo_in_samp = i.next().unwrap();
            enc_3.tick(Some([(*qlfo_in_samp + *val_samp) as f64].iter()));

            // TODO: compose this more artfully
            *v0.next().unwrap() = enc_3.next().unwrap_or(0.) as f32; // first audio stream
            *v1.next().unwrap() = enc_3.next().unwrap_or(0.) as f32; // second
            *v2.next().unwrap() = enc_3.next().unwrap_or(0.) as f32; // third
            *v3.next().unwrap() = enc_3.next().unwrap_or(0.) as f32; // fourth
            *v_trig.next().unwrap() = enc_3.next().unwrap_or(0.) as f32; // trigger
        }

        // update lights every block
        ring_tx.send((0, enc_0.get_leds())).unwrap();
        ring_tx.send((1, enc_1.get_leds())).unwrap();
        ring_tx.send((2, enc_2.get_leds())).unwrap();
        ring_tx.send((3, enc_3.get_leds())).unwrap();

        // here we'd try to receive program status updates in order to know when to stop processing
        // audio and clean up, but for brevity we'll assume the audio will process forever
        // (wouldn't that be nice)
        jack::Control::Continue
    };

    let process = jack::ClosureProcessHandler::new(dsp_cb);
    let active_client = client.activate_async((), process)?;

    Ok((delta_tx, ring_rx, Box::new(active_client)))
}
