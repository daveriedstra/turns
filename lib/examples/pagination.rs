extern crate ctrlc;
extern crate monome;
use monome::{Monome, MonomeEvent};

use crossbeam::channel::{self, Receiver, Sender};

use std::error::Error;
use std::io;
use std::{thread, time};

use turns_lib::encoder_modes::lev::Lev;
use turns_lib::encoder_modes::val::Val;
use turns_lib::encoder_mode::EncoderMode;
use turns_lib::ring::Ring;

fn main() -> Result<(), Box<dyn Error>> {
    println!("paged arc demo. press enter to change pages.");

    // handle sigint
    let (tx, rx) = channel::bounded(10);
    ctrlc::set_handler(move || {
        tx.send(()).unwrap();
    })?;

    // prep the arc
    let mut arc = Monome::new("/darc".to_string())?;
    for i in 0..4 {
        arc.ring_all(i, 0);
    }

    const REFRESH_IVAL_MS: usize = 100;
    let refresh_sr = 1000 / REFRESH_IVAL_MS;
    // make each page's encoders
    let mut page_0 = vec![
        Lev::new(refresh_sr, 1),
        Lev::new(refresh_sr, 1),
        Lev::new(refresh_sr, 1),
        Lev::new(refresh_sr, 1),
    ];
    let mut page_1 = vec![
        Val::new(refresh_sr, 1),
        Val::new(refresh_sr, 1),
        Val::new(refresh_sr, 1),
        Val::new(refresh_sr, 1),
    ];
    let mut rings = vec![Ring::new(0), Ring::new(1), Ring::new(2), Ring::new(3)];

    let mut on_lower_page = true; // a real application would have better logic

    // start terminal thread
    let (term_stop_tx, page_flip_rx) = start_term_thread();

    // main loop with timeout
    loop {
        // catch all arc poll events
        loop {
            match arc.poll() {
                Some(MonomeEvent::EncoderDelta { n, delta }) => {
                    // only perform updates for the active page
                    if on_lower_page {
                        page_0[n.clamp(0, 3)].delta(delta);
                    } else {
                        page_1[n.clamp(0, 3)].delta(delta);
                    }
                }
                _ => break,
            }
        }

        // tick and update lights
        // we're not consuming the values, just blinkenlights
        if on_lower_page {
            for (i, e) in page_0.iter_mut().enumerate() {
                e.tick(None);
                let leds = e.get_leds();
                rings[i].enqueue_all_aa(leds);
                rings[i].update(&mut arc);
            }
        } else {
            for (i, e) in page_1.iter_mut().enumerate() {
                e.tick(None);
                let leds = e.get_leds();
                rings[i].enqueue_all_aa(leds);
                rings[i].update(&mut arc);
            }
        }

        // check to see if we flipped pages; if so, do the flip
        if let Ok(()) = page_flip_rx.try_recv() {
            on_lower_page = !on_lower_page;
        }

        // if we get sigint, turn off the lights and exit, otherwise continue
        if let Ok(()) = rx.try_recv() {
            for i in 0..4 {
                arc.ring_all(i, 0);
            }
            term_stop_tx.try_send(())?;
            thread::sleep(time::Duration::from_millis(REFRESH_IVAL_MS as u64));
            return Ok(());
        } else {
            thread::sleep(time::Duration::from_millis(10));
        }
    }
}

fn start_term_thread() -> (Sender<()>, Receiver<()>) {
    let (term_tx, term_rx) = channel::bounded(10);
    let (page_tx, page_rx) = channel::bounded(10);

    std::thread::spawn(move || {
        // we need a reference to sdtin
        let stdin = io::stdin();
        let mut stdin_buf = String::new();

        loop {
            // handle stdin -- just toggle if there was an event
            if stdin.read_line(&mut stdin_buf).is_ok() {
                page_tx.try_send(()).unwrap();
            }

            if let Ok(()) = term_rx.try_recv() {
                break true;
            }

            thread::sleep(time::Duration::from_millis(100));
        }
    });

    (term_tx, page_rx)
}
