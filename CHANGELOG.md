# Changelog

The format of this log is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0] - 2022-03-30

Math, pager, and QOL improvements.

### Added

- `scale-octave` and `offset-hz` for simple pitch operations when working with exponential / pitch scale
- `mirror-shape` option
- MIDI pager now has four modes: `stepped-cc`, `stepped-value`, `addressed-cc`, and `addressed-value`. `stepped` and `addressed` still work as before but are aliased to the relevant new modes (`stepped-cc` and `addressed-value`). See README for more details.
- turns checks for turns.toml in current directory before using system config dir
- pager.py checks for turns.toml in current directory (but doesn't yet fall back to config dir)

### Changed

- Replaced scale-unit system with shape-transform system (see README for details)
- Fixed some math & terminology in shape
- Use `ring_map` instead of `ring_set`
- Corrected reference config files

## [0.4.0] - 2022-01-15

First public release.

### Added

* Changelog
* Paging and pager script (`pager.py`)
* Rotation
* Wave shapes for oscillating outputs
* Scale, offset, and unit configuration options
* Delta mode

### Changed

* Oscillating outputs are always unipolar (0-1)
* QuadLfo intersections don't necessarily match display
* MomentumOsc is more consistent across sample rates & block sizes
* Intersection detection overhauled
* Trigger outputs send a N ms gate instead of a single sample
* Trigger outputs send single KR messages instead of the outline of their AR envelope
* Unused transports don't start a thread (mainly AR transport)
* Use Rust 2021 edition
* Restructure to workspace

### Removed

* MomentumOsc tests
