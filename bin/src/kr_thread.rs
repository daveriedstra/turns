use std::{thread, time::Duration};
use std::net::{UdpSocket, SocketAddrV4};
use std::io::Error as IoError;
use std::str::FromStr;
use std::convert::{TryInto, TryFrom};

use anyhow::Result as AResult;
use crossbeam::channel;
use crossbeam::channel::{Sender, Receiver};

use helgoboss_midi::{RawShortMessage, ShortMessage, ShortMessageType, U7, FromBytesError, ControllerNumber};
use midir::{MidiOutput, MidiInput, MidiOutputConnection};

use rosc::{encoder, OscMessage, OscPacket, OscType, decoder::decode as rosc_decode};

use crate::config::turns_config::{TurnsConfig, PagerConfig, PagerTransport, PagerMidiMode};

pub type KrThreadHandles = (
    Sender<RawShortMessage>,
    Sender<OscMessage>,
    Receiver<PagerMsg>,
    Option<midir::MidiInputConnection<()>>
);

/**
 * Starts the Kr transport thread
 */
pub fn start_kr_transport_thread(cfg: &TurnsConfig) -> AResult<KrThreadHandles> {
    let (midi_tx, mut midi_rx) = channel::unbounded::<RawShortMessage>();
    let (osc_tx, mut osc_rx) = channel::unbounded::<rosc::OscMessage>();
    let (mut pager_tx, pager_rx) = channel::unbounded::<PagerMsg>();

    let refresh_rate: u64 = cfg.control_rate_refresh_ms.try_into()?;

    // Choose the first available midi output port and assume it works
    // @TODO - double check this approach
    let midi_dev_name = cfg.midi.device_name.clone();
    let midi_in = MidiInput::new(&midi_dev_name)?;
    let midi_out = MidiOutput::new(&midi_dev_name)?;
    let out_port = &midi_out.ports()[0];
    let mut conn_out = midi_out.connect(out_port, &cfg.midi.device_name).unwrap();

    // Try to bind to the specified OSC IP address and port -- if it fails, panic! at the disco
    // We only send to one IP:Port address for now, maybe in the future we'll support individual
    // addresses per encodermode...
    let send_from_addr = format!("{}:{}", cfg.osc.send_from_ip, cfg.osc.send_from_port);
    let send_from_addr = std::net::SocketAddrV4::from_str(&send_from_addr)?;
    let send_to_addr = format!("{}:{}", cfg.osc.send_to_ip, cfg.osc.send_to_port);
    let send_to_addr = SocketAddrV4::from_str(&send_to_addr)?;
    let send_osc_sock = UdpSocket::bind(&send_from_addr);
    println!("sending OSC events from {:?} to {:?}", &send_from_addr, &send_to_addr);

    // OSC receiving
    let listen_addr = format!("{}:{}", cfg.osc.listen_on_ip, cfg.osc.listen_on_port);
    let listen_addr = SocketAddrV4::from_str(&listen_addr)?;
    let listen_osc_sock = UdpSocket::bind(&listen_addr)?;
    println!("listening for OSC events on {:?}", &listen_addr);

    // clone only the config values needed in the KR thread, since we can't move it because it's
    // not 'static
    let pager_cfg = cfg.pager.clone();
    let pager_cc: u8 = pager_cfg.midi_cc.try_into()?;
    let pager_osc_addr = cfg.pager.osc_address.clone();
    dbg!(&cfg.pager.transport);

    // outgoing can share the same thread because the crossbeam communication is non-blocking
    thread::spawn(move || {
        loop {
            handle_outgoing_midi(&mut midi_rx, &mut conn_out);
            handle_outgoing_osc(&send_osc_sock, &mut osc_rx, &send_to_addr);
            thread::sleep(Duration::from_millis(refresh_rate));
        }
    });

    // incoming needs its own thread
    let mut midi_in_conn = None;
    match &pager_cfg.transport {
        PagerTransport::Midi => {
            // start listening for midi input here in order to keep the connection reference alive
            let in_port = &midi_in.ports()[0];
            let num_pages = cfg.pages.len();
            midi_in_conn = Some(midi_in.connect(in_port, &midi_dev_name, move |_, midi_msg, _| {
                handle_incoming_midi(midi_msg, &mut pager_tx, &pager_cfg, pager_cc, num_pages)
            }, ()).unwrap());
        },
        PagerTransport::Osc => {
            thread::spawn(move || {
                // translate messages from serialscthread to OSC
                loop {
                    let mut buf = [0u8; rosc::decoder::MTU];
                    match listen_osc_sock.recv_from(&mut buf) {
                        Ok((size, _addr)) => {
                            let packet = rosc_decode(&buf[..size]).unwrap();
                            handle_incoming_osc(packet, &mut pager_tx, &pager_osc_addr);
                        }
                        Err(e) => {
                            println!("Error receiving from socket: {}", e);
                        }
                    }
                }
            });
        }
        _ => {} // No pager
    };

    Ok((midi_tx, osc_tx, pager_rx, midi_in_conn))
}

/**
 * Send messages from a Crossbeam channel to a MIDI output connection
 */
fn handle_outgoing_midi(midi_rx: &mut Receiver<RawShortMessage>, conn_out: &mut MidiOutputConnection) {
    while let Ok(midi_msg) = midi_rx.try_recv() {
        let msg = [midi_msg.status_byte(), midi_msg.data_byte_1().get(), midi_msg.data_byte_2().get()];
        conn_out.send(&msg).unwrap();
    }
}

/**
 * Converts the raw short message bytes from the midir handler to a helgoboss RawShortMessage
 */
fn make_short_msg(midi_msg: &[u8]) -> Result<RawShortMessage, FromBytesError> {
    let msg_as_tuple = (midi_msg[0], U7::new(midi_msg[1]), U7::new(midi_msg[2]));
    RawShortMessage::try_from(msg_as_tuple)
}

/**
 * Handle incoming midi messages
 */
fn handle_incoming_midi(midi_msg: &[u8], pager_tx: &mut Sender<PagerMsg>, pager_cfg: &PagerConfig, cc: u8, num_pages: usize) {
    if let Ok(m) = make_short_msg(midi_msg) {
        let msg_type = m.r#type();
        if msg_type == ShortMessageType::ControlChange {
            handle_incoming_midi_cc(m, pager_tx, pager_cfg, cc, num_pages);
        }
    }
}

/**
 * Handle CC messages
 */
fn handle_incoming_midi_cc(msg: RawShortMessage, pager_tx: &mut Sender<PagerMsg>, pager_cfg: &PagerConfig, cc: u8, num_pages: usize) {
    let msg_cc = msg.controller_number().unwrap();
    let msg_value: usize = msg.control_value().unwrap().into();
    match pager_cfg.midi_mode {
        PagerMidiMode::AddressedCc => {
            // go to page determined by CC number
            // first, filter controllers that don't represent a page, being careful not to try and
            // hold a negative in an unsigned int.
            let msg_cc = msg_cc.get();
            if msg_cc >= cc && msg_cc - cc < num_pages as u8 { // target >= 0 and < num_pages
                if let Err(e) = pager_tx.send(PagerMsg::GoTo((msg_cc - cc) as usize)) {
                    println!("{}", e);
                }
            }
        },
        PagerMidiMode::AddressedValue => {
            // go to specified page
            if msg_cc == ControllerNumber::new(cc) {
                if let Err(e) = pager_tx.send(PagerMsg::GoTo(msg_value)) {
                    println!("{}", e);
                }
            }
        },
        PagerMidiMode::SteppedCc => {
            if msg_value < 1 { return; }; // stripnote

            // go down if specified CC, up if next one
            if msg_cc == ControllerNumber::new(cc) {
                if let Err(e) = pager_tx.send(PagerMsg::Previous) {
                    println!("{}", e);
                }
            } else if msg_cc == ControllerNumber::new(cc + 1) {
                if let Err(e) = pager_tx.send(PagerMsg::Next) {
                    println!("{}", e);
                }
            }
        },
        PagerMidiMode::SteppedValue => {
            if msg_cc == ControllerNumber::new(cc) {
                let pager_msg = if msg_value > 0 {
                    PagerMsg::Next
                } else {
                    PagerMsg::Previous
                };

                if let Err(e) = pager_tx.send(pager_msg) {
                    println!("{}", e);
                }
            }
        }
    }
}

/**
 * Send messages from a crossbeam channel to the appropriate OSC endpoint
 */
fn handle_outgoing_osc(send_osc_sock: &Result<UdpSocket, IoError>, osc_rx: &mut Receiver<OscMessage>, send_to_addr: &SocketAddrV4) {
    if let Ok(ref socket) = send_osc_sock {
        while let Ok(osc_msg) = osc_rx.try_recv() {
            let packet = OscPacket::Message(osc_msg);
            let msg_buf = encoder::encode(&packet).unwrap();
            socket.send_to(&msg_buf, send_to_addr).unwrap();
        }
    }
}

/**
 * Handle an incoming OSC packet
 */
fn handle_incoming_osc(packet: OscPacket, pager_tx: &mut Sender<PagerMsg>, _pager_osc_addr: &str) {
    if let OscPacket::Message(msg) = packet {
        match msg.addr.as_str() {
            "/turns/page" => {
                match &msg.args[0] {
                    OscType::Int(i) => {
                        if let Err(e) = pager_tx.send(PagerMsg::GoTo(*i as usize)) {
                            println!("{}", e);
                        }
                    },
                    OscType::String(s) => {
                        match s.to_lowercase().as_str() {
                            "next" => {
                                if let Err(e) = pager_tx.send(PagerMsg::Next) {
                                    println!("{}", e);
                                }
                            },
                            "prev" => {
                                if let Err(e) = pager_tx.send(PagerMsg::Previous) {
                                    println!("{}", e);
                                }
                            },
                            _ => {}
                        }
                    },
                    _ => {}
                }
            },
            _ => {}
        }
    }
}

pub enum PagerMsg {
    Next,
    Previous,
    GoTo(usize),
}
