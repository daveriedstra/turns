// #![allow(dead_code)]
// #![allow(unused_imports)]

extern crate helgoboss_midi;
extern crate ctrlc;
extern crate turns_lib;
extern crate midir;
extern crate crossbeam;

mod config;

mod kr_mode_with_transport;
mod ar_mode_with_transport;
mod kr_thread;
mod ar_thread;
mod configured_encoder;

use crate::config::get_config::get_config;
use crate::config::turns_config::EncoderTransportOption;
use crate::configured_encoder::{configure_encoder, EncoderWithTransport};
use crate::kr_thread::{start_kr_transport_thread, PagerMsg};
use crate::ar_thread::{ToDspMsg, FromDspMsg, start_jack_client};
use turns_lib::ring::{Ring, RingLed};

use monome::{Monome, MonomeEvent};

use std::{thread, time::Duration, convert::TryInto};

use crossbeam::channel;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // handle sigint
    let (tx, rx) = channel::bounded(10);
    ctrlc::set_handler(move || {
        tx.send(()).unwrap();
    })?;

    let config = get_config()?;
    println!("turning... ");

    // get arc handle and configure
    let mut arc = Monome::new("/darc".to_string())?;
    for i in 0..4 {
        arc.ring_all(i, 0);
    }
    // the number of leds to add to accomplish rotation
    let rotation_addition = ((config.rotate_deg / -90 + 4) % 4) * 64 / 4;

    // we'll have up to three threads and at least two:
    // - main thread which talks to the arc
    // - (optional) kr thread
    // - (optional) ar thread
    //
    // only the main thread knows which page is active; the kr and ar threads
    // are always sending on their respective transports so that whatever processes in the encoder
    // modes don't seem to stop (don't want your lfo stopping while you're on some other page)

    let (midi_tx, osc_tx, pager_rx, _midi_in_conn) = start_kr_transport_thread(&config)?;
    let mut has_ar_encoders = false;
    let mut has_kr_encoders = false;

    let mut pages: Vec<Vec<EncoderWithTransport>> = config.pages.iter()
        .map(|page| {
            page.encoders.iter()
                .map(|e| {
                    match e.transport {
                        EncoderTransportOption::Cv => has_ar_encoders = true,
                        EncoderTransportOption::Midi | EncoderTransportOption::Osc => has_kr_encoders = true,
                    }
                    configure_encoder(e, &config, &midi_tx, &osc_tx)
                })
                .collect()
        })
        .collect();
    let mut active_page = 0;
    let num_pages = pages.len();

    let mut rings = vec![Ring::new(0), Ring::new(1), Ring::new(2), Ring::new(3)];

    // start DSP thread
    let dsp_handles = if has_ar_encoders {
        Some(start_jack_client(&mut pages)?)
    } else {
        None
    };

    //
    // main loop
    //

    // also used in kr thread
    let kr_refresh_ms:u64 = config.control_rate_refresh_ms.try_into()?;
    loop {
        // catch all arc events and update relevant active Kr EncoderModes
        while let Some(MonomeEvent::EncoderDelta { n, delta }) = arc.poll() {
            let l = pages[active_page].len();
            let enc = &mut pages[active_page][n.clamp(0, l-1)];
            match enc {
                EncoderWithTransport::Kr(ref mut krs) => krs.mode.delta(delta),
                EncoderWithTransport::Ar(_ars) => {
                    // send delta to Ar thread
                    if let Some((ref dsp_tx, _, _)) = dsp_handles {
                        dsp_tx.send(ToDspMsg::Delta {
                            page: active_page,
                            n,
                            delta,
                        })?;
                    }
                },
            }
        }

        // update Kr EncoderModes and their Rings on this thread
        for (i_page, page) in pages.iter_mut().enumerate() {
            for (i_enc, enc) in page.iter_mut().enumerate() {
                if let EncoderWithTransport::Kr(kr) = enc {
                    kr.update();
                    if i_page == active_page {
                        let leds = kr.mode.get_leds();
                        update_ring(&mut arc, &mut rings[i_enc], leds, rotation_addition);
                    }
                }
            }
        }

        // get messages from Dsp thread, including RingLed updates from Ar encoder modes
        if let Some((_, ref dsp_rx, _)) = dsp_handles {
            while let Ok(msg) = dsp_rx.try_recv() {
                match msg {
                    FromDspMsg::Ring{ page, n, ring } => {
                        if page == active_page {
                            update_ring(&mut arc, &mut rings[n], ring, rotation_addition);
                        }
                    }
                };
            }
        }

        // paging
        while let Ok(pager_msg) = pager_rx.try_recv() {
            match pager_msg {
                PagerMsg::Previous => {
                    active_page = (active_page + num_pages - 1) % num_pages;
                },
                PagerMsg::Next => {
                    active_page = (active_page + 1) % num_pages;
                },
                PagerMsg::GoTo(p) => {
                    active_page = p.clamp(0, num_pages);
                },
            }
        }

        // if we get sigint, turn off the lights and exit, otherwise continue
        if let Ok(()) = rx.try_recv() {
            for i in 0..4 {
                arc.ring_all(i, 0);
            }
            if let Some((ref dsp_tx, _, _)) = dsp_handles {
                dsp_tx.send(ToDspMsg::Quit)?;
            }
            thread::sleep(Duration::from_millis(100));
            println!("turned.");
            return Ok(());
        } else {
            thread::sleep(Duration::from_millis(kr_refresh_ms));
        }
    }
}

/**
 * Updates the ring and applies rotation.
 */
fn update_ring(arc: &mut Monome, ring: &mut Ring, mut leds: Vec<RingLed>, rotation_addition: isize) {
    for l in &mut leds {
        l.0 = l.0 + rotation_addition as f64 % 64.;
    }
    ring.enqueue_all_aa(leds);
    ring.update(arc);
}
