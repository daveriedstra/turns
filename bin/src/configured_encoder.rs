use helgoboss_midi::{ControllerNumber, RawShortMessage};
use rosc::OscMessage;

use crossbeam::channel::Sender;

use crate::config::turns_config::{TurnsConfig, EncoderConfig, EncoderModeOption, EncoderTransportOption};
use crate::kr_mode_with_transport::{KrModeWithTransport, send_midi_cc, send_osc_to};
use crate::ar_mode_with_transport::ArModeWithTransport;

use turns_lib::encoder_mode::EncoderMode;
use turns_lib::encoder_modes::{delta::Delta, dial::Dial, dual_lfo::DualLfo, lev::Lev,
    lfo::Lfo, quad_lfo::QuadLfo, seq::Seq, val::Val};
use turns_lib::wave_shape::WaveShape;

/**
 * Creates and configures an EncoderMode with the appropriate transport
 */
pub fn configure_encoder<'a>(enc_cfg: &EncoderConfig, cfg: &TurnsConfig, midi_tx: &'a Sender<RawShortMessage>, osc_tx: &'a Sender<OscMessage>)
    -> EncoderWithTransport<'a>
{
    let kr_refresh_ival_ms = cfg.control_rate_refresh_ms;
    let kr_rate = 1000 / kr_refresh_ival_ms;

    // These are overridden in start_jack_client with the actual values,
    // but it makes sense to default to sensible, likely values.
    let ar_sample_rate = 44100;
    let ar_block_size = 64;

    match enc_cfg.transport {
        EncoderTransportOption::Cv => {
            let mode = match_encoder(enc_cfg, ar_sample_rate, ar_block_size);
            let arm = ArModeWithTransport::new(mode, enc_cfg.port_name.clone().unwrap());
            EncoderWithTransport::Ar(arm)
        },
        EncoderTransportOption::Midi => {
            // TODO: note vs CC
            let mut mode = match_encoder(enc_cfg, kr_rate, 1);
            let channel = enc_cfg.midi_channel.unwrap();
            let cc_num = enc_cfg.midi_cc_number.unwrap();
            let senders = (0..mode.get_output_count()).map(|i| {
                let cc = (cc_num + i as u8).clamp(0, 127);
                send_midi_cc(channel, ControllerNumber::new(cc), midi_tx)
            }).collect();
            EncoderWithTransport::Kr(KrModeWithTransport::new(mode, senders))
        },
        EncoderTransportOption::Osc => {
            let osc_address = enc_cfg.osc_address.as_ref().unwrap().to_string();
            let mut mode = match_encoder(enc_cfg, kr_rate, 1);
            let n_outputs = mode.get_output_count();
            // if there are multiple outputs, append the output index to the address
            let senders = if n_outputs == 1 {
                vec![send_osc_to(osc_address, osc_tx)]
            } else {
                (0..n_outputs).map(|i| {
                    let addr = format!("{}-{}", osc_address, i);
                    send_osc_to(addr, osc_tx)
                }).collect()
            };
            EncoderWithTransport::Kr(KrModeWithTransport::new(mode, senders))
        }
    }
}

/**
 * This is a bit of a weird type: Kr modes are wrapped in a KrModeWithTransport configured to send their
 * output to the correct threads, and Ar modes are more or less bare. This allows us to collect
 * them all generically and then separate them out into Kr and Ar modes as appropriate when we boot
 * up the threads.
 *
 * The Ar modes are a bit of a special case because they have to be fully moved to the audio
 * thread.
 */
pub enum EncoderWithTransport<'a> {
    Kr(KrModeWithTransport<'a>),
    Ar(ArModeWithTransport)
}

/**
 * This is where all the mode-specific option setting takes place. After this, the modes are all
 * treated interchangeably.
 */
fn match_encoder(cfg: &EncoderConfig, sr: usize, bs: usize) -> Box<dyn EncoderMode<Item=f64> + Send> {
    match cfg.mode {
        EncoderModeOption::Value => {
            let mut m = Val::new(sr, bs);
            m.shape = cfg.get_shape();
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
        EncoderModeOption::Seq => Box::new(Seq::new(sr, bs)) as Box<dyn EncoderMode<Item=f64> + Send>,
        EncoderModeOption::Level => {
            let mut m = Lev::new(sr, bs);
            m.shape = cfg.get_shape();
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
        EncoderModeOption::Lfo => {
            let mut m = Lfo::new(sr, bs);
            // Set wave shape
            m.set_wave_shape(WaveShape::from(&cfg.wave_shape));
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
        EncoderModeOption::DualLfo => {
            let mut m = DualLfo::new(sr, bs);
            // Set wave shape
            m.set_wave_shape(WaveShape::from(&cfg.wave_shape));
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
        EncoderModeOption::QuadLfo => {
            let mut m = QuadLfo::new(sr, bs);
            // Set wave shape
            m.set_wave_shape(WaveShape::from(&cfg.wave_shape));
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
        EncoderModeOption::Delta => {
            let mut m = Delta::new(sr, bs);
            m.shape = cfg.get_shape();
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
        EncoderModeOption::Dial => {
            let mut m = Dial::new(sr, bs);
            if let Some(s) = cfg.num_stops {
                m.set_num_stops(s);
            }
            Box::new(m) as Box<dyn EncoderMode<Item=f64> + Send>
        },
    }
}
