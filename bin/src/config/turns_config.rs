use serde::Deserialize;
use anyhow::Result as AResult;
use turns_lib::{wave_shape::WaveShape, shape::{Shape, Transformation}};

const APP_NAME_PLACEHOLDER: &str = "{app}";
const MODE_PLACEHOLDER: &str = "{mode}";
const PAGE_PLACEHOLDER: &str = "{page}";
const ENC_PLACEHOLDER: &str = "{enc}";

const DEF_PORT_NAME: &str = "{mode}-{page}-{enc}";
const DEF_OSC_ADDR: &str = "/{app}/page_{page}/enc_{enc}_{mode}";


//
// Structure of the config file
//

/**
 * Applies a template to an encoder-specific string.
 * Meant for port names and OSC addresses.
 */
pub fn apply_str_template(s: &mut Option<String>, default: &str, mode: &str, page_idx: usize, enc_idx: usize)
    -> String
{
    let mut target = match s.as_mut() {
        Some(n) => n.clone(),
        None => "".to_string(),
    };

    if target.is_empty() {
        target = default.to_string();
    }

    for (placeholder, value) in [
        (APP_NAME_PLACEHOLDER, "turns".to_string()),
        (MODE_PLACEHOLDER, mode.to_lowercase()),
        (PAGE_PLACEHOLDER, page_idx.to_string()),
        (ENC_PLACEHOLDER, enc_idx.to_string()),
    ] {
        target = target.replace(placeholder, &value);
    }

    target
}

/**
 * If a config struct or enum has defaults, call this to apply them.
 */
pub trait Validates {
    fn validate(&mut self) -> AResult<()>;
}

fn rotate_deg_default() -> isize { 0 }
fn control_rate_refresh_ms_default() -> usize { 1 }

#[derive(Debug, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct TurnsConfig {
    #[serde(default="rotate_deg_default")]
    pub rotate_deg: isize,
    #[serde(default="control_rate_refresh_ms_default")]
    pub control_rate_refresh_ms: usize,
    #[serde(default)]
    pub pager: PagerConfig,
    #[serde(default)]
    pub osc: OscConfig,
    #[serde(default)]
    pub midi: MidiConfig,
    #[serde(default)]
    pub jack: JackConfig,
    #[serde(alias="page", default)]
    pub pages: Vec<Page>,
    #[serde(alias="encoder", default)]
    pub encoders: Vec<EncoderConfig>,
}

impl Validates for TurnsConfig {
    fn validate(&mut self) -> AResult<()> {
        self.rotate_deg = match self.rotate_deg {
            -270 | -180 | -90 | 0 | 90 | 180 | 270 => self.rotate_deg,
            _ => {
                let msg = "Geometry is hard, I can only rotate in 90 degree increments. (Only values accepted are 0, 90, 180, and 270 and their negatives.)";
                return Err(anyhow::Error::msg(msg));
            }
        };
        self.control_rate_refresh_ms = self.control_rate_refresh_ms.max(10);
        self.osc.validate()?;

        if !self.encoders.is_empty() {
            // single page config takes precedence
            self.pages = vec!(Page {
                encoders: self.encoders.clone()
            })
        }

        // validate each encoder configurations, agnostic to page for now
        self.pages.iter_mut()
            .map(|page| &mut page.encoders)
            .flatten()
            .try_for_each(|enc| enc.validate())?;

        self.apply_default_names();

        Ok(())
    }
}

impl TurnsConfig {
    // iterate through all encoders and apply default names to ports and addresses
    fn apply_default_names(&mut self) {
        let pages_enumerator = self.pages.iter_mut().enumerate();
        let mut port_names = vec![];
        let mut osc_addresses = vec![];
        let mut midi_channel_ccs = vec![];

        for (page_idx, page) in pages_enumerator {
            let enc_enumerator = page.encoders.iter_mut().enumerate();

            for (enc_idx, enc) in enc_enumerator {
                let mode_name = format!("{:?}", enc.mode);
                match enc.transport {
                    EncoderTransportOption::Cv => {
                        let final_port_name = apply_str_template(&mut enc.port_name, DEF_PORT_NAME,
                            &mode_name, page_idx, enc_idx);
                        if port_names.contains(&final_port_name) {
                            println!("Duplicate Jack port name detected, assuming you know what you're doing.");
                            println!("{}", &final_port_name);
                        }
                        port_names.push(final_port_name.clone());
                        enc.port_name.replace(final_port_name);
                    },
                    EncoderTransportOption::Osc => {
                        let final_osc_addr = apply_str_template(&mut enc.osc_address, DEF_OSC_ADDR,
                            &mode_name, page_idx, enc_idx);
                        if osc_addresses.contains(&final_osc_addr) {
                            println!("Duplicate OSC address detected, assuming you know what you're doing.");
                            println!("{}", &final_osc_addr);
                        }
                        osc_addresses.push(final_osc_addr.clone());
                        enc.osc_address.replace(final_osc_addr);
                    },
                    EncoderTransportOption::Midi => {
                        // Log the MIDI channel and CC number
                        let chan = enc.midi_channel.get_or_insert_with(Default::default);
                        let cc = enc.midi_cc_number.get_or_insert_with(Default::default);

                        // If the channel & cc number have already been logged, increment the CC
                        if midi_channel_ccs.contains(&(*chan, *cc)) {
                            *cc = *cc + 1;
                        }

                        midi_channel_ccs.push((chan.clone(), cc.clone()));
                    }
                }
            }
        }
    }
}

// Configuration for turns pager
#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct PagerConfig {
    #[serde(default="PagerConfig::transport_default")]
    pub transport: PagerTransport,
    #[serde(default="PagerConfig::midi_cc_default")]
    pub midi_cc: usize,
    #[serde(default="PagerConfig::midi_mode_default")]
    pub midi_mode: PagerMidiMode,
    #[serde(default="PagerConfig::osc_address_default")]
    pub osc_address: String,
}

impl Default for PagerConfig {
    fn default() -> Self {
        PagerConfig {
            transport: PagerConfig::transport_default(),
            midi_cc: PagerConfig::midi_cc_default(),
            midi_mode: PagerConfig::midi_mode_default(),
            osc_address: PagerConfig::osc_address_default(),
        }
    }
}

impl PagerConfig {
    pub fn transport_default() -> PagerTransport { PagerTransport::None }
    pub fn midi_cc_default() -> usize { 0 }
    pub fn midi_mode_default() -> PagerMidiMode { PagerMidiMode::SteppedCc }
    pub fn osc_address_default() -> String { "/turns/page".to_string() }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct Page {
    #[serde(alias = "encoder", default)]
    pub encoders: Vec<EncoderConfig>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all="kebab-case")]
pub enum PagerTransport {
    Midi,
    Osc,
    Tui,
    None,
}

#[derive(Debug, Deserialize, Clone, Copy)]
#[serde(rename_all="kebab-case")]
pub enum PagerMidiMode {
    /// The incoming MIDI CC controller number determines the number of the target page.
    /// The difference between the incoming number and the default number is the page that will be
    /// flipped to.
    AddressedCc,
    /// The incoming MIDI CC value is the number of target page
    #[serde(alias = "addressed")]
    AddressedValue,
    /// The incoming MIDI CC controller number determines the step direction.
    /// Non-zero values on specified CC flip to previous page, non-zero values on the next CC flip
    /// to next page
    #[serde(alias = "stepped")]
    SteppedCc,
    /// The incoming MIDI CC value determines the step direction.
    SteppedValue,
}

// Configuration for turns OSC
#[derive(Debug, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct OscConfig {
    #[serde(default="OscConfig::send_from_ip_default")]
    pub send_from_ip: String,
    #[serde(default="OscConfig::send_from_port_default")]
    pub send_from_port: usize,
    #[serde(default="OscConfig::send_to_ip_default")]
    pub send_to_ip: String,
    #[serde(default="OscConfig::send_to_port_default")]
    pub send_to_port: usize,

    #[serde(default="OscConfig::listen_on_ip_default")]
    pub listen_on_ip: String,
    #[serde(default="OscConfig::listen_on_port_default")]
    pub listen_on_port: usize,
}

impl Validates for OscConfig {
    fn validate(&mut self) -> AResult<()> {
        self.send_from_ip = self.send_from_ip.replace("localhost", "127.0.0.1");
        self.send_to_ip = self.send_to_ip.replace("localhost", "127.0.0.1");
        self.listen_on_ip = self.listen_on_ip.replace("localhost", "127.0.0.1");
        Ok(())
    }
}

impl Default for OscConfig {
    fn default() -> Self {
        OscConfig {
            send_from_ip: OscConfig::send_from_ip_default(),
            send_from_port: OscConfig::send_from_port_default(),
            send_to_ip: OscConfig::send_to_ip_default(),
            send_to_port: OscConfig::send_to_port_default(),
            listen_on_ip: OscConfig::listen_on_ip_default(),
            listen_on_port: OscConfig::listen_on_port_default(),
        }
    }
}

impl OscConfig {
    pub fn send_from_ip_default() -> String { "localhost".to_string() }
    pub fn send_from_port_default() -> usize { 8080 }
    pub fn send_to_ip_default() -> String { "localhost".to_string() }
    pub fn send_to_port_default() -> usize { 8081 }
    pub fn listen_on_ip_default() -> String { "localhost".to_string() }
    pub fn listen_on_port_default() -> usize { 8082 }
}

// Configuration for turns MIDI
#[derive(Debug, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct MidiConfig {
    #[serde(default="MidiConfig::device_name_default")]
    pub device_name: String,
}

impl Default for MidiConfig {
    fn default() -> Self {
        MidiConfig {
            device_name: MidiConfig::device_name_default(),
        }
    }
}

impl MidiConfig {
    fn device_name_default() -> String { "turns".to_string() }
}

// Configuration for turns Jack client
#[derive(Debug, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct JackConfig {
    #[serde(default="JackConfig::device_name_default")]
    pub device_name: String,
}

impl Default for JackConfig {
    fn default() -> Self {
        JackConfig {
            device_name: JackConfig::device_name_default(),
        }
    }
}

impl JackConfig {
    fn device_name_default() -> String { "turns".to_string() }
}

fn scale_x_default() -> f64 { 1. }
fn scale_y_default() -> f64 { 1. }

// Configuration for an arc encoder
#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all="kebab-case")]
pub struct EncoderConfig {
    pub mode: EncoderModeOption,
    pub transport: EncoderTransportOption,
    pub port_name: Option<String>,
    pub out_min: Option<f64>,
    pub out_max: Option<f64>,
    pub in_min: Option<f64>,
    pub in_max: Option<f64>,
    pub midi_mode: Option<EncoderMidiMode>,
    pub midi_cc_number: Option<u8>,
    pub midi_channel: Option<u8>,
    pub osc_address: Option<String>,

    // input shaping function
    #[serde(default)]
    #[serde(alias="scale")]
    pub shape: EncoderShape,
    #[serde(default)]
    pub mirror_shape: bool,

    #[serde(default="scale_x_default")]
    pub scale_x: f64,
    #[serde(default="scale_y_default")]
    pub scale_y: f64,
    #[serde(default)]
    pub offset_x: f64,
    #[serde(default)]
    pub offset_y: f64,

    pub offset_hz: Option<f64>,
    #[serde(alias="scale-8ve")]
    pub scale_octave: Option<f64>,

    // mode-specific options
    #[serde(default)]
    pub wave_shape: EncoderWaveShape,
    pub num_stops: Option<usize>,
}

impl EncoderConfig {
    /// Returns a Shape represented by this config
    pub fn get_shape(&self) -> Shape {
        let t = Transformation {
            offset_x: self.offset_x,
            offset_y: self.offset_y,
            scale_x: self.scale_x,
            scale_y: self.scale_y,
        };

        let s = match self.shape {
            EncoderShape::Linear => Shape::Linear(t),
            EncoderShape::Squared => Shape::Squared(t),
            EncoderShape::Exponential => Shape::Exponential(t),
            EncoderShape::Logarithmic => Shape::Logarithmic(t),
        };

        if self.mirror_shape {
            Shape::MirroredNegative(Box::new(s))
        } else {
            s
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all="kebab-case")]
pub enum EncoderModeOption {
    Value,
    Seq,
    Level,
    Lfo,
    DualLfo,
    QuadLfo,
    Dial,
    Delta,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all="kebab-case")]
pub enum EncoderTransportOption {
    Cv,
    Midi,
    Osc,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all="kebab-case")]
pub enum EncoderMidiMode {
    Cc,
    HiResCc,
    Note
}

/**
 * Config option for an encoder mode's wave shape.
 */
#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all="kebab-case")]
pub enum EncoderWaveShape {
    Square,
    Triangle,
    Sine,
    SawRising,
    SawFalling,
}

impl Default for EncoderWaveShape {
    fn default() -> Self {
        Self::Sine
    }
}

impl From<&EncoderWaveShape> for WaveShape {
    fn from(ews: &EncoderWaveShape) -> Self {
        match ews {
            EncoderWaveShape::Square => WaveShape::Square,
            EncoderWaveShape::Triangle => WaveShape::Triangle,
            EncoderWaveShape::Sine => WaveShape::Sine,
            EncoderWaveShape::SawRising => WaveShape::SawRising,
            EncoderWaveShape::SawFalling => WaveShape::SawFalling,
        }
    }
}

/**
 * Config option for an encoder mode's output scaling.
 */
#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(rename_all="kebab-case")]
pub enum EncoderShape {
    #[serde(alias="lin")]
    Linear,
    #[serde(alias="expo")]
    #[serde(alias="pitch")]
    Exponential,
    #[serde(alias="log")]
    Logarithmic,
    Squared,
}

impl Default for EncoderShape {
    fn default() -> Self {
        Self::Linear
    }
}

impl Validates for EncoderConfig {
    fn validate(&mut self) -> AResult<()> {
        // apply offset-hz for exponential shape
        if self.shape == EncoderShape::Exponential {
            if let Some(scale_8ve) = self.scale_octave {
                if scale_8ve == 0. {
                    panic!("Can't use an octave scale of zero");
                }
                self.scale_x = scale_8ve.powf(-1.);
            }
            if let Some(offset_hz) = self.offset_hz {
                // determine appropriate offset_x and offset_y here
                self.offset_y = 0.;
                self.offset_x = -1. * offset_hz.log2() * self.scale_x;
            }
        }

        // Shorthand: expo & log scales normalized to start at 0 if no scale or offset specified
        if self.scale_x == 1. && self.scale_y == 1. && self.offset_x == 0. && self.offset_y == 0. {
            match self.shape {
                EncoderShape::Exponential => self.offset_y = -1.,
                EncoderShape::Logarithmic => self.offset_x = -1.,
                _ => {},
            }
        }

        // port name handled in apply_str_template
        // duplicate CCs handled in apply_default_names
        match self.transport {
            EncoderTransportOption::Midi => {
                self.midi_channel.get_or_insert(0);
                let midi_mode = self.midi_mode.get_or_insert(EncoderMidiMode::Cc);
                let cc_num_min = helgoboss_midi::ControllerNumber::MIN.get();
                let cc_num_max = helgoboss_midi::ControllerNumber::MAX.get();
                match midi_mode {
                    EncoderMidiMode::Cc => {
                        inner_clamp(&mut self.midi_cc_number, cc_num_min, cc_num_max, 0);
                        inner_clamp_float(&mut self.out_min, 0., 127., 0.);
                        inner_clamp_float(&mut self.out_max, 0., 127., 127.);
                        inner_clamp_float(&mut self.in_min, 0., 127., 0.);
                        inner_clamp_float(&mut self.in_max, 0., 127., 127.);
                        inner_clamp(&mut self.num_stops, 0, 127, 0);
                    },
                    EncoderMidiMode::HiResCc => {
                        // TODO - 14bit logic
                        inner_clamp(&mut self.midi_cc_number, cc_num_min, cc_num_max, 0);
                        inner_clamp_float(&mut self.out_min, 0., 16383., 0.);
                        inner_clamp_float(&mut self.out_max, 0., 16383., 16383.);
                        inner_clamp_float(&mut self.in_min, 0., 16383., 0.);
                        inner_clamp_float(&mut self.in_max, 0., 16383., 16383.);
                        inner_clamp(&mut self.num_stops, 0, 16383, 0);
                    },
                    EncoderMidiMode::Note => {
                        self.midi_cc_number = None;
                        inner_clamp_float(&mut self.out_min, 0., 127., 0.);
                        inner_clamp_float(&mut self.out_max, 0., 127., 127.);
                        inner_clamp_float(&mut self.in_min, 0., 127., 0.);
                        inner_clamp_float(&mut self.in_max, 0., 127., 127.);
                        inner_clamp(&mut self.num_stops, 0, 127, 0);
                    },
                };
            },
            EncoderTransportOption::Osc => {
                // osc_address handled in apply_str_template(
            },
            EncoderTransportOption::Cv => {
            },
        }

        Ok(())
    }
}

/**
 * Clamps the inner value of a non-float numeric Option in-place and supplies a default
 */
fn inner_clamp<T: Ord + Copy>(x: &mut Option<T>, min: T, max: T, default: T) {
    let val = x.unwrap_or(default).clamp(min, max);
    x.get_or_insert(val);
}

/**
 * Clamps the inner value of a float Option in-place and supplies a default
 */
fn inner_clamp_float<T: Into<f64> + From<f64> + Copy>(x: &mut Option<T>, min: T, max: T, default: T) {
    let val = x.unwrap_or(default)
        .into()
        .clamp(min.into(), max.into());
    x.get_or_insert(val.into());
}
