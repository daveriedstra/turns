use std::{fs, path};
use clap::{App, Arg};
use directories_next::ProjectDirs;
use anyhow::Result as AResult;

use crate::config::turns_config::{TurnsConfig, Validates};

/**
 * Get the config described by the commandline arguments and the config file they refer to.
 */
pub fn get_config() -> AResult<TurnsConfig> {
    let results = App::new("turns")
        .version("0.3.0")
        .author("Dave Riedstra <dave.riedstra@gmail.com")
         .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .value_name("FILE")
            .help("Sets a custom config file")
            .takes_value(true))
         .about("CLI client for the Monome Arc")
        .get_matches();

    let conf_file = get_current_config_file(results.value_of("config"))?;
    build_config(conf_file)
}

/**
 * Validates the provided conf file and applies any defaults as needed
 */
fn build_config(conf_file: String) -> AResult<TurnsConfig> {
    let mut read_cfg: TurnsConfig = toml::from_str(&conf_file)?;
    read_cfg.validate()?;
    // dbg!(&read_cfg);
    Ok(read_cfg)
}

/**
 * Returns the contents of the requested config file
 */
fn get_current_config_file(cli_config_path: Option<&str>) -> AResult<String> {
    // get explicit file path
    if let Some(c) = cli_config_path {
        let file_path = path::PathBuf::from(c);
        fs::read_to_string(&file_path)
            .map_err(|e| {
                let msg = format!("Could not open requested config file ({:?}):\n{}", file_path, e);
                anyhow::Error::msg(msg)
            })
    } else if let Ok(conf) = fs::read_to_string("./turns.toml") {
        // get file in current directory
        Ok(conf)
    } else {
        // get default config
        let file_path = get_default_conf_path()?;
        fs::read_to_string(file_path)
            .map_err(|e| {
                let msg = format!("Could not open default config file:\n{}", e);
                anyhow::Error::msg(msg)
            })
    }
}

/**
 * Returns the path for the default configuration on this platform.
 */
pub fn get_default_conf_path() -> AResult<path::PathBuf> {
    if let Some(platform_dirs) = ProjectDirs::from("com", "daveriedstra", "turns") {
        Ok(platform_dirs.config_dir().join("config.toml"))
    } else {
        Err(anyhow::Error::msg("Error finding default conf location (are you using a supported OS?)"))
    }
}
