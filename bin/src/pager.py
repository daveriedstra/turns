#!/usr/bin/env python3
'A very simple terminal interface for paging turns'

# arrow keys & vim keys for up/down

# would-be-nice list:
# - incremental qwerty left hand as grid for jumping to a layer
#   (should display layer key)
# - custom layer shortcut keys

import curses
import argparse
import toml
from pythonosc import udp_client
from pathlib import Path

MASTHEAD='pager for turns. ↑↓/kj to step through. q to quit.'


def main(stdscr):
    # init curses window
    stdscr.clear()
    curses.curs_set(0)
    curses.start_color()
    curses.use_default_colors()

    # init values
    args = parse_args()
    names = get_layer_names(args)
    n_layers = len(names)
    active_layer = 0
    osc_client = udp_client.SimpleUDPClient(args.ip, args.port)
    osc_address = args.address

    # render loop
    display(stdscr, names)
    while True:
        c = stdscr.getkey()
        if c == 'j' or c == 'KEY_DOWN':
            active_layer = (active_layer + n_layers - 1) % n_layers
        elif c == 'k' or c == 'KEY_UP':
            active_layer = (active_layer + 1) % n_layers
        elif c == 'q' or c == 'ESC':
            break
        osc_client.send_message(osc_address, active_layer)
        display(stdscr, names, active_layer)


def parse_args():
    opt = argparse.ArgumentParser(description=__doc__)
    opt.add_argument('-f', '--file', help='turns toml config file to use.')
    opt.add_argument('-c', '--count', default=0, type=int, help='Number of layers. Superseded by --names.')
    opt.add_argument('-n', '--names', default='', nargs='+', help='A list of layer names. Supersedes --count.')
    opt.add_argument('-i', '--ip', default='127.0.0.1', help='IP address turns is listening on.')
    opt.add_argument('-p', '--port', default=8082, type=int, help='Port turns is listening on.')
    opt.add_argument('-a', '--address', default='/turns/page', help='OSC address turns is listening on.')
    return opt.parse_args()


def get_layer_names(args):
    '''get a list of layer names according to the configuration
    '''
    if args.file != None:
        return get_layer_names_from_turns_conf(args.file)
    elif Path("./turns.toml").is_file():
        return get_layer_names_from_turns_conf("./turns.toml")
    elif len(args.names) > 0:
        return args.names
    else:
        return range(0, args.count)

def get_layer_names_from_turns_conf(fname):
        # get file
        turns_cfg = toml.load(fname)
        # reduce to name or index
        names = []
        for i, page in enumerate(turns_cfg['page']):
            name = page['name'] if 'name' in page else i
            names.append(name)
        return names


def display(stdscr, names, active_layer=0):
    stdscr.addstr(0, 2, MASTHEAD)
    l = len(names)
    for i, name in enumerate(names):
        y = 1 + l - i # list from bottom to top
        if i == active_layer:
            stdscr.addstr(y, 0, '* {}'.format(name))
        else:
            stdscr.addstr(y, 0, '  {}'.format(name))
    stdscr.refresh()

curses.wrapper(main)

# args = parse_args()
# get_layer_names(args)
