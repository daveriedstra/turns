use crossbeam::channel::Sender;
use helgoboss_midi::{Channel, ControllerNumber, KeyNumber, RawShortMessage,
    ShortMessageFactory, U7};
use turns_lib::encoder_mode::EncoderMode;

/**
 * This struct associates an EncoderMode, a set of function closures responsible for sending its
 * data to control-rate protocols, and some state to determine whether new values should be sent.
 */
pub struct KrModeWithTransport<'a> {
    pub mode: Box<dyn EncoderMode<Item=f64> + Send>,
    pub do_send: Vec<Box<dyn Fn(f64) + 'a>>,
    pub prev: Vec<f64>,
}

impl<'a> KrModeWithTransport<'a> {
    pub fn new(mode: Box<dyn EncoderMode<Item=f64> + Send>, do_send: Vec<Box<dyn Fn(f64) + 'a>>) -> Self {
        let num_channels = do_send.len();
        KrModeWithTransport {
            mode,
            do_send,
            prev: (0..num_channels).map(|_| 0.).collect(),
        }
    }

    pub fn update(&mut self) {
        self.mode.tick(None);

        // Send each of the output values to its associated lambda if it's been updated.
        let mut i = 0;
        while let Some(next) = self.mode.next() {
            let prev = self.prev[i];

            // call the associated closure for this value
            if (prev - next).abs() > f64::EPSILON {
                (self.do_send[i])(next);
            }

            self.prev[i] = next;
            i += 1;
        }
    }
}

/**
 * A factory to make closures to send MIDI notes to the provided crossbeam channel
 * TODO: finish option to send midi notes
 */
pub fn _send_midi_note(ch: u8, midi_tx: &'_ Sender<RawShortMessage>)
    -> Box<dyn Fn(f64) + '_>
{
    Box::new(move |samp: f64| {
        let msg = RawShortMessage::note_on(
            Channel::new(ch),
            KeyNumber::new((samp * 127.).clamp(0., 127.) as u8),
            U7::new(127_u8)
        );

        if let Err(e) = midi_tx.send(msg) {
            dbg!(e);
        };
    })
}


/**
 * A factory to make closures to send MIDI CC messages to the provided crossbeam channel
 */
pub fn send_midi_cc(ch: u8, cc_number: ControllerNumber, midi_tx: &'_ Sender<RawShortMessage>)
    -> Box<dyn Fn(f64) + '_>
{
    Box::new(move |samp: f64| {
        let msg = RawShortMessage::control_change(
            Channel::new(ch),
            cc_number,
            U7::new((samp * 127.).clamp(0., 127.) as u8)
        );

        if let Err(e) = midi_tx.send(msg) {
            dbg!(e);
        };
    })
}

/**
 * A factory for making closures to send OSC messages to the provided crossbeam channel
 */
pub fn send_osc_to(addr: String, osc_tx: &'_ Sender<rosc::OscMessage>)
    -> Box<dyn Fn(f64) + '_>
{
    Box::new(move |samp: f64| {
        let msg = rosc::OscMessage {
            addr: addr.to_owned(),
            args: vec!(rosc::OscType::Float(samp as f32)),
        };

        if let Err(e) = osc_tx.send(msg) {
            dbg!(e);
        };
    })
}
