use turns_lib::encoder_mode::EncoderMode;

use jack::{Port, AudioOut, AudioIn};
use anyhow::Result as AResult;

/**
 * This struct associates an EncoderMode with its Jack ports and the crossbeam channels to be used
 * for communication between the DSP thread and the main thread. It's expected that the EncoderMode
 * and the Jack ports will be moved to the DSP thread.
 */
pub struct ArModeWithTransport {
    pub mode: Option<Box<dyn EncoderMode<Item=f64> + Send>>,
    out_port_names: Vec<String>,
    in_port_names: Vec<String>,
    pub jack_output_ports: Option<Vec<Port<AudioOut>>>,
    pub jack_input_ports: Option<Vec<Port<AudioIn>>>,
}

impl ArModeWithTransport {
    pub fn new(mut mode: Box<dyn EncoderMode<Item=f64> + Send>, port_name: String) -> ArModeWithTransport {
        let out_port_count = mode.get_output_count();
        let in_port_count = mode.get_input_count();

        ArModeWithTransport {
            mode: Some(mode),
            out_port_names: (0..out_port_count).map(|n| format!("{}_out_{}", port_name, n)).collect(),
            in_port_names: (0..in_port_count).map(|n| format!("{}_in_{}", port_name, n)).collect(),
            jack_output_ports: None,
            jack_input_ports: None,
        }
    }

    pub fn make_out_ports(&mut self, jack_client: &mut jack::Client) -> AResult<()>  {
        let p = self.out_port_names.iter_mut()
            .map(|name| {
                jack_client.register_port(name, AudioOut::default())
            })
            .collect::<Result<Vec<_>, _>>()?; // some rust magic nonsense here

        self.jack_output_ports = Some(p);
        Ok(())
    }

    pub fn make_in_ports(&mut self, jack_client: &mut jack::Client) -> AResult<()>  {
        let p = self.in_port_names.iter_mut()
            .map(|name| {
                jack_client.register_port(name, AudioIn::default())
            })
            .collect::<Result<Vec<_>, _>>()?;

        self.jack_input_ports = Some(p);
        Ok(())
    }
}
