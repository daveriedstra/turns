use turns_lib::ring::{RingLed};
use turns_lib::encoder_mode::EncoderMode;
use crate::configured_encoder::EncoderWithTransport;

use anyhow::Result as AResult;
use crossbeam::channel;

pub enum ToDspMsg {
    Quit,
    Delta {
        page: usize,
        n: usize,
        delta: i32,
    },
}

pub enum FromDspMsg {
    Ring {
        page: usize,
        n: usize,
        ring: Vec<RingLed>,
    }
}

/// What gets returned when the DSP thread is initialized:
/// - a channel to send messages TO the DSP thread
/// - a channel to receive messages FROM the DSP thread
/// - a reference to the jack client which needs to be maintained for the life of the thread
pub type DspThreadHandles = (
    channel::Sender<ToDspMsg>,
    channel::Receiver<FromDspMsg>,
    Box<dyn std::any::Any>,
);

struct DspEncoder {
    mode: Box<dyn EncoderMode<Item=f64> + Send>,
    out_ports: Vec<jack::Port<jack::AudioOut>>,
    in_ports: Vec<jack::Port<jack::AudioIn>>,
    i_page: usize,
    i_enc: usize,
}

/**
 * Configure and run the DSP.
 */
pub fn start_jack_client(pages: &mut Vec<Vec<EncoderWithTransport>>) -> AResult<DspThreadHandles>
{
    let (mut client, _) = jack::Client::new("turns", jack::ClientOptions::NO_START_SERVER)?;
    let buffer_size = client.buffer_size() as usize;
    let sample_rate = client.sample_rate() as usize;

    // A map of everything that will be moved to the DSP thread, including the page and encoder indices
    let mut encoders_map = pages.iter_mut()
        .enumerate()
        .map(|(i_page, page)| {
            page.iter_mut()
                .enumerate()
                .map(|(i_enc, enc)| {
                    if let EncoderWithTransport::Ar(ars) = enc {
                        if let Some(ref mut mode) = ars.mode {
                            mode.set_sample_rate(sample_rate);
                            mode.set_block_size(buffer_size);
                        }
                        ars.make_out_ports(&mut client).ok()?;
                        ars.make_in_ports(&mut client).ok()?;
                        Some(DspEncoder {
                            mode: ars.mode.take()?,
                            out_ports: ars.jack_output_ports.take().or(Some(vec![]))?,
                            in_ports: ars.jack_input_ports.take().or(Some(vec![]))?,
                            i_page,
                            i_enc,
                        })
                    } else {
                        None
                    }
                })
                .collect()
        })
        .collect::<Vec<Vec<Option<DspEncoder>>>>();

    // engage our two-way communication
    let (delta_tx, delta_rx) = channel::unbounded();
    let (ring_tx, ring_rx) = channel::unbounded();

    let dsp_cb = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        // get delta values from arc
        while let Ok(msg) = delta_rx.try_recv() {
            match msg {
                ToDspMsg::Quit => {
                    return jack::Control::Quit;
                }
                ToDspMsg::Delta { page, n, delta } => {
                    if let Some(ref mut enc) = &mut encoders_map[page][n] {
                        enc.mode.delta(delta);
                    };
                }
            }
        }

        // for each encoder on each page,
        // get buffer references
        // pass on input buffers
        // fill output buffers
        // update lights
        encoders_map
            .iter_mut()
            .flatten()
            .filter_map(|e| e.as_mut())
            .for_each(|e| {
                let mut out_buffs = e.out_ports.iter_mut()
                    .map(|p| p.as_mut_slice(ps).iter_mut())
                    .collect::<Vec<_>>(); // TODO - refactor to get these vecs off the heap every cycle
                let in_buffs = e.in_ports.iter_mut()
                    .map(|p| p.as_slice(ps).iter()) // ports to buffers
                    .map(|mut b| *b.next().unwrap() as f64) // buffers to samples
                    .collect::<Vec<_>>();

                for _ in 0..buffer_size {
                    // process input samples
                    e.mode.tick(Some(in_buffs.iter()));

                    // and write generated output samples to buffer
                    for b in out_buffs.iter_mut() {
                        if let Some(sample) = b.next() {
                            *sample = e.mode.next().unwrap_or(0.) as f32;
                        }
                    }
                }

                // after doing the audio, send a ring update
                ring_tx.send(FromDspMsg::Ring {
                    page: e.i_page,
                    n: e.i_enc,
                    ring: e.mode.get_leds(),
                }).unwrap();
            });

        jack::Control::Continue
    };

    let process = jack::ClosureProcessHandler::new(dsp_cb);
    let active_client = client.activate_async((), process)?;

    Ok((delta_tx, ring_rx, Box::new(active_client)))
}
