# turns

An app for using the Monome Arc on your computer. Supports CV (via [jack audio](https://jackaudio.org/)), MIDI, OSC, and exposes the useful bits in `turns-lib`.

![demo video](https://daveriedstra.com/video/turns-quick-demo-640.vp9.webm)

**Work in progress.** This project is under active development, and I welcome feature requests and bug reports, please file them on the [GitLab repository](https://gitlab.com/daveriedstra/turns). Check [TODO.md](TODO.md) if you're interested in seeing what work I'd like to do. There is also a [change log](CHANGELOG.md). Turns follows [semantic versioning](https://semver.org/), which for users of the app means that breaking changes in behaviour are generally confined to "major" releases (eg 1.x.x or 2.x.x) and backwards-compatible new features will be added in "minor" releases (eg, x.5.x). While turns is in 0.x.x the API is technically unstable, so check back here if something is behaving unexpectedly after an update.

## Requirements

turns requires a working [serialosc](https://monome.org/docs/serialosc/osc/) installation.

Theoretically, turns works on Linux, Mac, and Windows, but is currently only tested on Linux. I don't have ready access to a Windows or Mac machine, but if you file a bug or PR I'll have a look.

## Installation

64-bit Linux builds are available on the [Releases page](https://gitlab.com/daveriedstra/turns/-/releases). For now users of other platforms will have to clone & compile turns with [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) until I can get cross-compiling set up. (Keep in mind that jack must be installed before building the project.)

## Usage

turns configures the arc encoders independently based on a TOML config file:

```
turns --config config.toml
```

When called with no arguments, turns will look for a default config file in the current directory, and if it doesn't find one, it'll look in the default config location for your OS:

* **Linux:** `$XDG_CONFIG_HOME/turns/config.toml`
* **Mac:** `/Users/Username/Library/Application Support/com.daveriedstra.turns/config.toml`
* **Windows:** `C:\Users\Username\AppData\Roaming\daveriedstra\turns\config\config.toml`

```
turns
```

## Configuration

Configuration is currently documented in the [default](bin/config.default.toml), [example](bin/config.example.toml), and [minimal](bin/config.minimal.toml) config files.

turns is designed to be very flexible: use it with your Pure Data patches, control MIDI instruments with it, use it as a modulation source for hardware synths, or control anything with OSC. Here are some configuration tips.

### Controlling software with CV

jack allows flexible routing and doesn't clip audio streams, which means you can use them as CV for extremely precise and low-latency control. For instance, turns can control the cutoff of a filter in Pure Data by connecting the output of a Level mode to pd, then connecting the appropriate `[input~]` to the frequency control of the filter. With `shape = "pitch"`, `offset-hz = 20`, and `scale-octave = 10`, one Level mode rotation will correspond to the entire audio range (and no scaling will need to be done in pd).

### Controlling hardware with CV

turns is very handy with a DC-coupled audio interface as an output, but keep in mind that turns' output will be scaled to the voltage output of the interface, so an output of `1` from turns does not necessarily mean the interface is sending 1 volt. If you know the voltage range of your output, you can configure turns to match it for more predictable results. For example, if your interface sends +/- 5 volts, setting `scale-y = 0.2` for a Value mode will mean that each full rotation sends 1 volt (using the default linear shape).

### Controlling synths with MIDI

turns can send output over MIDI to control hardware and software synths. The output is scaled to 0-127 by default for MIDI transports. Support for MIDI notes and 14-bit resolution is in the works.

### Controlling anything with OSC

turns allows configuring OSC to point to any IP address and port through the global `[osc]` config section and lets you configure the OSC address per encoder. This flexibility means that in addition to controlling sound-making applications, turns can also be used for high-precision control over, for instance, a graphics editor or a motor via a WiFi connection to a Raspberry Pi.

## Encoder Modes

turns will set up one or more pages of encoder modes based on the configuration. These will output values on whatever transport they're configured to use. More than one transport can be active simultaneously, but each mode will only use one transport. A mode might have more than one output, and eventually might be able to use inputs as well.

The following modes are available (with these defaults):

### Level

A simple finite value. Useful for volume controls.

```toml
[[encoder]]
mode = "level"
shape = "linear"
offset-x = 0
offset-y = 0
scale-x = 1
scale-y = 1
```

### Value

A simple infinite value. One full rotation is 0-1, and the number of rotations are shown with notches after noon.

```toml
[[encoder]]
mode = "value"
shape = "linear"
offset-x = 0
offset-y = 0
scale-x = 1
scale-y = 1
```

### Delta

Like the raw delta from serialosc but scaled. Will eventually have configurable slew and curve.

```toml
[[encoder]]
mode = "delta"
shape = "linear"
offset-x = 0
offset-y = 0
scale-x = 1
scale-y = 1
```

### Lfo

A single LFO. Set wave shape with the `wave-shape` option.

```toml
[[encoder]]
mode = "lfo"
wave-shape = "sine"
```

### DualLfo

Two LFOs controlled by encoder momentum in either direction.

```toml
[[encoder]]
mode = "dual-lfo"
wave-shape = "sine"
```

### QuadLfo

Four sine LFOs, two each controlled by momentum in either direction. The second runs at half the rate of the first. An additional output sends triggers whenever the LFOs intersect. Set wave shape with the `wave-shape` option.

```toml
[[encoder]]
mode = "quad-lfo"
wave-shape = "sine"
```

### Dial

Striated output for a certain number of steps.

```toml
[[encoder]]
mode = "dial"
num-stops = 0
```

### Seq

A rotary event sequencer. Not yet very useful because its input feature isn't implemented.

```toml
[[encoder]]
mode = "seq"
```

### Shapes, ranges, and precision

Some of turns' modes offer fine control over the relationship between the encoder input and the value it modifies. turns treats this as a geometric operation in which the input value (number of encoder turns) is processed by a function before being applied to whatever it's modifying (often the Mode's output or the frequency of its oscillation). The basic functions are

* linear (or "lin", default): `f(x) = x`
* squared: `f(x) = x²`
* logarithmic (or "log"): `f(x) = log2(x + 1)`
* exponential (or "expo" or "pitch"): `f(x) = 2ˣ`

These functions can be modified geometrically with `offset-x`, `offset-y`, `scale-x`, and `scale-y`. When `mirror-shape = true` is set, the "negative" input (counterclockwise from the initial position) will generate the same curve as the "positive" (clockwise) input but flipped on both X and Y axes. This can provide a more useful or intuitive interface for non-linear shapes like `expo` and `log`, but can produce unexpected shapes with `squared` and is totally useless with `linear` -- use your judgement.

There are some shorthands to help simplify this math a bit:

- `offset-hz` can be used with `expo` / `pitch` to define the starting pitch.
- `scale-octave` can be used with `expo` / `pitch` to define the number of octaves covered by a rotation.
- when no other offset or scale arguments are given, `log` and `expo` / `pitch` are shifted so that the first full rotation corresponds to 0 - 1.
- otherwise, offsets default to 0 and scales default to 1.

These modes currently support input shaping:

- Level
- Value
- Delta

(This is an area which I'd like to improve and I welcome comments and suggestions.)

### Wave shape options

* sine (default)
* square
* triangle
* saw-rising (◢)
* saw-falling (◣)

## Paging

turns can be configured to use any number of pages, which can be navigated via OSC or MIDI. The pager and its transport (OSC or MIDI) must be explicitly enabled in the config. Improvements are planned in this area, check TODO.md if you're curious.

### MIDI Paging

The MIDI pager has four modes to accommodate various hardware & software configurations. Two of the modes are *stepped* (the incoming message steps to the next or previous adjacent page) and two of the modes are *addressed* (the incoming message determines the address of the page to jump to, keep in mind that the pages are zero-indexed so page `1` is the second page). Both types can use either the *CC number* or the *message value* to determine what to do. In other words,

- In `stepped-cc` mode (alias `stepped`, default in MIDI mode), the **CC** determines the step direction
    - An incoming high value (anything greater than 0) on the specified CC (default: 2) will step to the previous page
    - A high value on the next CC (eg, 3) will step to the next page
    - Useful for next / previous page **buttons**
- In `stepped-value` mode, the **value** of a single CC determines the step direction
    - An incoming high value on the specified CC (default: 2) will step to the next page
    - An incoming low value on the specified CC will step to the previous page
    - Useful for stepping in **software**
- In `addressed-cc` mode, the **CC** determines the address
    - An incoming high value on the specified CC (default: 2) will flip to page 0
    - An incoming high value on the next CC (eg, 3) will flip to page 1
    - An incoming high value on the nth-next CC will flip to page n (eg, CC 4 will flip to page 2, etc.)
    - Useful for **one-button-per-page**
- In `addressed-value` mode (alias `addressed`), the **value** of a single CC determines the step
    - An incoming value on the specified CC will flip to the corresponding page (eg, an incoming `0` will flip to page 0, a `1` will flip to page 1, etc.)
    - Useful for flipping pages with an **encoder**

Select the pager's midi mode with `midi-mode` and the relevant controller number with `midi-cc` in the `[pager]` section:

```toml
[pager]
transport = "midi"
midi-mode = "stepped-cc"
midi-cc = 2
```

### OSC Paging

The OSC pager accepts messages at `/turns/page` by default. A `next` or `prev` message will flip to the next or previous page respectively, and an incoming integer will flip to the page at that index. Keep in mind that the pages are zero-indexed so page `1` is the second page.

```toml
[pager]
transport = "osc"
osc-address = "/turns/page"
```

## pager.py

A simple pager terminal interface is included in [bin/src/pager.py](bin/src/pager.py). You'll need to have [python-osc](https://pypi.org/project/python-osc/)  and [toml](https://pypi.org/project/toml/) installed for it to work (`pip install python-osc toml`). It accepts the following arguments:

* `-f` / `--file`: turns toml config file to use. The pager will generate the appropriate pages and labels from the config.
* `-c` / `--count`: The number of pages to use in a manual configuration. Superseded by `--names`. e.g., calling `pager.py -c 3` will run a pager that lets you flip through 3 nameless pages.
* `-n` / `--names`: A list of page names. Supersedes `--count`. Keep in mind that spaces separate names, but you can include a space in a name by quoting the name. e.g., calling `pager.py -n filters lfos "amplitude modulation"` will run a pager that lets you flip through 3 pages called "filters", "lfos", and "amplitude modulation".
* `-i` / `--ip`: The IP address turns is listening on. Default is turns' default, `127.0.0.1` (ie, localhost).
* `-p` / `--port`: The port turns is listening on. Default is turns' default, `8082`.
* `-a` / `--address`: The OSC address turns is listening for page changes on. Default is `/turns/page`.
